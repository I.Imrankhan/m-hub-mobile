import React, { Component } from 'react'
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, AsyncStorage } from 'react-native'
import Image from 'react-native-scalable-image';

import { Actions } from 'react-native-router-flux';
let {height, width} = Dimensions.get('window');

export default class NavBar extends Component {
    logout = () => {
        AsyncStorage.removeItem('user')
        Actions.login()
    }
    render() {
        return(
            <View style = {styles.container}>
                <Image
                    width = {width / 3.5}
                    style = {{
                        // width: width / 3.7,
                        // height: height / 16,
                        marginRight: width / 3.5
                    }}
                    source = {require('../../assets/vena-logo.png')}
                />
                <Text style = {{color: '#4cd137', fontSize: 20, fontWeight: 'bold', marginLeft: -(width/7) }}>mHub</Text>
                {/*<TouchableOpacity*/}
                {/*    style = {styles.logout}*/}
                {/*    onPress={()=>this.logout()}*/}
                {/*>*/}
                {/*    <Text style = {{color: 'white'}}>Log out</Text>*/}
                {/*</TouchableOpacity>*/}
                {
//                <TouchableOpacity>
//                    <Image style = {{height: 30}} source = {require('../../assets/menu.png')} />
//                </TouchableOpacity>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: .05,
        padding: 15,
        paddingLeft: 5,
        backgroundColor: 'white',
        height: 100,
        flexDirection: 'row',
       // justifyContent: 'space-between',
        alignItems: 'center'
    },
    logout: {
        padding: 10,
        paddingRight: 30,
        paddingLeft: 30,
        borderRadius:  20,
        backgroundColor: '#007bff'
    }
})