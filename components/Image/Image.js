import React, { Component } from 'react'
import { View, Image, ActivityIndicator, StyleSheet } from 'react-native'
import { Actions } from 'react-native-router-flux';


class ShowImage extends Component {
    state = {
        img: '',
    }
    componentDidMount() {
         fetch('http://52.66.213.147:3000/api/controlCenter/getServiceOrderListById/'+this.props.hCode)
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        img: responseJson.data[0].link
                    })
                })
                .catch((error) => {
                    console.error(error);
                });
    }
   render() {

        if(this.state.img.length == 0) {
            return (
                <View style = {styles.horizontal}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        }
      return (
         <View style = {styles.container}>
             <Image style={{height: '60%',width: '100%'}} source = {{uri:this.state.img}}/>
         </View>
      )
   }
}
export default ShowImage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    horizontal: {
           position:  'absolute',
           top: '43%',
           left: '45%'
         },

         serviceImg: {

         }

})