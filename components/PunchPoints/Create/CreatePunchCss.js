import { StyleSheet } from 'react-native'

export default StyleSheet.create({

    container: {
//       alignItems: 'center',
       padding: 20,
       marginTop: 40
     },

     submitButton: {
       width: '100%',
       backgroundColor: '#007bff',
       paddingTop: 5,
       paddingBottom: 7,
       textAlign: 'center',
       marginBottom: 15,
       borderRadius: 5
     },

     submitButtonText:{
       color: 'white',
       fontSize: 20,
       textAlign : 'center'
     },

     buttonDisabled: {
       backgroundColor: '#ccc',
       color: '#666'
     },

     buttonEnabled: {
       color: '#fff',
       backgroundColor: '#007bff'
     },

     input: {
       width: '100%',
       marginBottom: 15,
       padding: 10,
       borderColor : '#aeb7bf',
       borderWidth: 1,
       borderRadius: 3
     },


      punchPoints: {
       backgroundColor: 'white',
       borderRadius: 10
      },

      punchPoint: {
       padding: 10,

      },

     heading: {
       fontSize: 20,
       padding: 10,
     },

     card: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 20,
        shadowColor: '#000',
        shadowOffset: { width: 10, height: 10 },
        shadowOpacity: .7,
        shadowRadius: 2,
        elevation: 5,
        padding: 10
     },

     row: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 40
     },

     rowCenter: {
        alignItems: 'center'
     } ,

     text: {
        fontSize: 15,
        flex: 1,
        flexWrap: 'wrap'
     },

     title :{
        width: 150,
        fontSize: 16,
        fontWeight: 'bold'
     },

     checkListContainer: {
        backgroundColor: 'white',
        padding: 10,
        paddingTop: 30,
        borderRadius: 5,
     },

     checkList: {
        borderColor: '#aeb7bf',
        borderWidth: 1
     },

     header: {
        backgroundColor: 'white',
        width: '50%',
        alignItems: 'center',
        padding: 10,
        marginTop: 30,
     },

     checkListTitle:{
        width: 100,
        fontWeight: 'bold'
     },

     checkListText: {
        width: 250
     },

     updatePunch: {
        margin: 10,
        marginRight: 5,
        marginLeft: 5
     },

     submitButton: {
        backgroundColor: '#007bff',
        paddingTop: 5,
        paddingBottom: 7,
        textAlign: 'center',
        marginBottom: 15,
//        height: 40,
        borderRadius: 5,
     },

     submitButtonText:{
        color: 'white',
        fontSize: 20,
        textAlign : 'center'
     },
})