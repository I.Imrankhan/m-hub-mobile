import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';

import obj from '../../config.js'
import styles from './CreatePunchCss.js'
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
//import { RNCamera } from 'react-native-camera';
import OfflineNotice from '../../OfflineNotice/OfflineNotice.js'

export default class CreatePunch extends Component {
  state = {
    points: '',
    punchPoints: [],
    fileName: '',
    photos: [],
    chooseFile: false
  }

  handlePoints = (text) => {
    this.setState({
        points: text,
    })
  }

  componentDidMount() {
    this.fetchPunchPoints()
  }

  selectFile = async() => {
      const results = await DocumentPicker.show(
          {
            filetype: [DocumentPickerUtil.allFiles()],
          },
          (error, res) => {
            this.setState({ fileUri: res.uri });
            this.setState({ fileType: res.type });
            this.setState({ fileName: res.fileName });
            this.setState({ fileSize: res.fileSize });
            this.savePunchPointAttachment()
          }
      );


  }

  savePunchPointAttachment = async () => {
     punchPointId = this.state.punchPoints[0].id
     data = new FormData();
     data.append("file", {
        uri: this.state.fileUri,
        type: "image/*",
        name: this.state.fileName,
     });

     if(obj.isConnected) {
         fetch(obj.BASE_URL+"api/controlCenter/SavePunchPointAttachment/"+punchPointId, {
            method: 'POST',
            headers: new Headers({
                "Accept": "application/x-www-form-urlencoded",
            }),
            body: data
         })
         .then((response) => response.text())
         .then((responseText) => {
            responseObj = JSON.parse(responseText)
            alert(responseObj.message)
         })
         .catch((error) => {
            alert(error);
         });
     }
  }

  savePunchPoints = () => {
        if(obj.isConnected) {
            fetch(obj.BASE_URL+"api/controlCenter/createPunchPoints", {
                  method: 'POST',
                  headers: new Headers({
                    'Content-Type': 'application/json',
                  }),
                  body: JSON.stringify({
                    points: this.state.points,
                    asset_id: this.props.asset_id,
                    plant_id_fk: this.props.plant_id,
                    user_hcode: obj.user_hCode
                  })
                })
                .then((response) => response.text())
                .then((responseText) => {
                    responseObj = JSON.parse(responseText)
                    alert(responseObj.message)
                    this.fetchPunchPoints()
                    this.setState({
                        chooseFile: true
                    })
                })
                .catch((error) => {
                    console.error(error);
                });
        }
  }



  fetchPunchPoints = () => {
        if(obj.isConnected) {
            fetch(obj.BASE_URL+"api/controlCenter/getPunchPoints/"+this.props.asset_id)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    punchPoints: responseJson.data
                })
            })
            .catch((error) => {
                console.error(error);
            });
        }
  }

  updatePunchPoints = (index) => {
        if(obj.isConnected) {
            fetch(obj.BASE_URL+"api/controlCenter/updatePunchPoints/"+this.state.punchPoints[index].hcode, {
                method: 'POST',
                    headers: new Headers({
                            'Content-Type': 'application/json',
                    }),

                    body: JSON.stringify({
                            "points": this.state.punchPoints[index].points,
                            "user_hcode": obj.user_hCode,
                    })
            })
            .then((response) => response.text())
            .then((responseText) => {
                responseObj = JSON.parse(responseText)
                    alert(responseObj.message)
                    this.fetchPunchPoints()
                })
                .catch((error) => {
                    console.log(error);
                });
        }
  }


  render() {
    disable = true
    punchPoints = false
    if(this.state.points.length)
        disable = false

    if(this.state.punchPoints.length)
        punchPoints = true
    return (
      <View>
        <OfflineNotice />
          <ScrollView keyboardShouldPersistTaps={'handled'}>
              <View style={styles.container}>
                <View style = {{flex: 1, flexDirection: 'row', alignItems: 'center', marginBottom: 20}}>
                    <Text style = {{fontSize: 20, color: '#4cd137'}}>Asset : </Text>
                    <Text style = {{fontSize: 20}} >{this.props.asset_name}</Text>
                </View>
                <TextInput style = {styles.input}
                    underlineColorAndroid = "transparent"
                    placeholder = "Enter punch points"
                    autoCapitalize = "none"
                    onChangeText = {this.handlePoints}
                />

                <View style = {{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                    {
                        this.state.chooseFile &&
                        <TouchableOpacity
                            onPress = {() => this.selectFile()}
                            style = {[styles.submitButton, {width: 120, paddingLeft: 10, marginRight: 20} ]}
                        >
                          <Text style = {{color: 'white'}}>Chooose File</Text>
                        </TouchableOpacity>
                    }
                    {
                        this.state.fileName.length != 0 &&
                        <Text style = {{marginTop: -10}}>
                            {this.state.fileName}
                        </Text>
                    }
                </View>

                <TouchableOpacity
                    disabled={disable}
                    style = {
                        [
                            styles.submitButton,
                            disable ? styles.buttonDisabled : styles.buttonEnabled, {marginTop: 20}
                        ]
                    }
                    onPress = {
                        () => this.savePunchPoints()
                    }
                >
                    <Text style = {styles.submitButtonText}>Save</Text>
                </TouchableOpacity>


                {
                    punchPoints &&
                    <View>
                        <View style = {styles.header}>
                            <Text style = {{fontSize: 18, fontWeight: 'bold'}}>
                                Punch Points
                            </Text>
                        </View>

                        <View style = {[styles.checkListContainer, styles.margin]}>
                            {
                                this.state.punchPoints.map((point,index) =>(
                                        <View style = {[styles.card, styles.margin, styles.checkList]} key = {index}>
                                            <View >
                                                <TextInput style = {styles.input}
                                                    underlineColorAndroid = "transparent"
                                                    autoCapitalize = "none"
                                                    value = {point.points}
                                                    onChangeText={(value) => {
                                                        copyData = Object.assign({}, this.state);
                                                        copyData.punchPoints[index].points = value;
                                                        this.setState({
                                                            punchPoints: copyData.punchPoints
                                                        })
                                                    }}
                                                />
                                                <TouchableOpacity
                                                    style = {[styles.submitButton, styles.updatePunch]}
                                                        onPress = {
                                                            () => this.updatePunchPoints(index)
                                                        }
                                                >
                                                    <Text style = {styles.submitButtonText}>Update</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                ))
                            }
                        </View>

                    </View>
                }

              </View>
          </ScrollView>
        </View>
        );
  }
}
