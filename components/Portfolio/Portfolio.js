import React, {Component} from 'react'
import {
    View,
    ScrollView,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    BackHandler,
    Alert,
    Dimensions, ActivityIndicator
} from 'react-native'
import BASE_URL from '../config.js'
import commonStyles from '../stylesCss'
import OfflineNotice from '../OfflineNotice/OfflineNotice.js'
import obj from '../config.js'
import firebase from "firebase";
import styles from "./PortfolioCss";
import TableRow from './TableRow'
import {Actions} from "react-native-router-flux";

let {height, width} = Dimensions.get('window')

class Portfolio extends Component {

    ref;
    state = {
        assetsData: [],
        searchText: '',
        isLoading: true,
        sortOrder: 1,
        current: '',
        previous: '',
        text: 'STOP',
        sortBy: '',
        // assets: {'asd':'asdas','asdsa':'asda','asdas':'asdas','asdasasd':'asasdas','aasdas':'asdas'},
        liveDataStatus: 1
    }

    getPlants = async () => {
        let plants = [];
        await fetch(obj.BASE_URL + "api/controlCenter/getAssetByUserId/" + obj.user_hCode)
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.data.length) {
                    responseJson.data.map((item) => {
                        plants.push(item.plant_name.toLowerCase())
                    })
                } else {
                    this.setState({
                        dataNotFound: true
                    })
                }
            })
        this.setState({
            plants: plants
        })

        // console.log('state data')
        // console.log(this.state.plants)
        this.getPlantLiveData('off')
    }

    getPlantLiveData = (status) => {
        let assets = {}
        let assetsData = []
        let scope = this

        if (status == 1) {
            scope.setState({
                liveDataStatus: 0,
                text: 'START'
            })
        } else if (status == 0) {
            scope.setState({
                liveDataStatus: 1,
                text: 'STOP'
            })
        }

        let gamesaPlants = ['tgp1'
            , 'fatanpur'
            , 'patan'
            , 'tgp2'
            , 'jath'
            , 'jmd'
            , 'pililla'
        ]
        let vestasPlants = ['mangoli1']
        let plantsLiveData = {}

        let config = {
            apiKey: "AIzaSyDI4so_sVXzjAjFMLWRy8nVuH6HDtageNE",
            authDomain: "170271322381-eqbedbns3pi77vhg5c44gidh6kks0dns.apps.googleusercontent.com",
            databaseURL: "https://energon-fb-production.firebaseio.com",
            projectId: "energon-fb-production",
            storageBucket: "energon-fb-production.appspot.com",
            // messagingSenderId: "*********"
        }
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
        let db = firebase.database();
        scope.ref = db.ref("realtime/");
        let listener = scope.ref.on("value", async function (snapshot) {
            // console.log('inside ref')
            snapshot.forEach((plant) => {
                if ((scope.state.plants.includes(plant.key) && (gamesaPlants.includes(plant.key) || vestasPlants.includes(plant.key))) || plant.key == 'mangoli1') {
                    let tag;

                    let total_power_gen = 0;
                    let total_temperature = 0;
                    let total_wind_speed = 0;

                    let temp_asset_count = 0;
                    let speed_asset_count = 0;
                    let gen_asset_count = 0;
                    let wind_direction = 0;
                    let total_asset = 0;
                    let grand_pw_gen = 0;
                    let avg_temperature = 0;
                    let avg_wind_speed = 0;
                    let power_gen_MB = 0;
                    let grand_pow_in_MB = 0;
                    let PLF = 0;
                    let avg_wind_direction = 0;
                    let operational = 0;
                    let waitingForWind = 0;
                    let turbineDown = 0;
                    let emergency = 0;
                    plant.forEach((item) => {

                        if (gamesaPlants.includes(plant.key)) {
                            tag = "Estado"
                            power_gen2 = +(item.val()['Gen,Potencia']['tag_value']);
                            temp2 = +(item.val()['Amb,Temp']['tag_value']);
                            wspeed2 = +(item.val()['Amb,VelViento']['tag_value']);
                            wdirection2 = +(item.val()['Amb,Direccion']['tag_value']);

                            if ((new Date(item.val()[tag]['created_at']) < new Date(item.val()['ErrorComunicaciones']['created_at']))) {
                                if (item.val().ErrorComunicaciones.tag_value == 'true') {
                                    emergency++;
                                }
                            } else if ((parseInt(item.val()[tag].tag_value) == 125)) {
                                emergency++;
                            } else if (parseInt(item.val()[tag].tag_value) == 100 || item.val()[tag].tag_value == "undefined") {
                                operational++;
                            } else if (parseInt(item.val()[tag].tag_value) == 75) {
                                waitingForWind++;
                            } else if (parseInt(item.val()[tag].tag_value) == 0 || parseInt(item.val()[tag].tag_value) == 25 || parseInt(item.val()[tag].tag_value) == 50) {
                                turbineDown++;
                            }
                        } else if (vestasPlants.includes(plant.key)) {
                            tag = "System,OperationStateInt"
                            power_gen2 = +(item.val()['Grid,Power']['tag_value']);
                            temp2 = +(item.val()['Ambient,Temperature']['tag_value']);
                            wspeed2 = +(item.val()['Ambient,WindSpeed']['tag_value']);
                            wdirection2 = +(item.val()['Ambient,WindDir']['tag_value']);

                            if (parseInt(item.val()[tag].tag_value) == 0) {
                                emergency++;
                            } else if (parseInt(item.val()[tag].tag_value) == 3) {
                                operational++;
                            } else if (parseInt(item.val()[tag].tag_value) == 2) {
                                waitingForWind++;
                            } else if (parseInt(item.val()[tag].tag_value) == 1) {
                                turbineDown++;
                            }
                        }


                        grand_pw_gen = +power_gen2;

                        wind_direction += wdirection2;
                        if (power_gen2 > 0) {
                            total_power_gen += power_gen2;
                            gen_asset_count++;
                        }
                        if (temp2 > 0) {
                            total_temperature += temp2;
                            temp_asset_count++;
                        }
                        if (wspeed2 > 0) {
                            total_wind_speed += wspeed2;
                            speed_asset_count++;
                        }
                        total_asset++;
                    })
                    avg_temperature = total_temperature / temp_asset_count;
                    avg_wind_speed = total_wind_speed / speed_asset_count;
                    power_gen_MB = total_power_gen / 1000;
                    grand_pow_in_MB = grand_pw_gen / 1000;
                    PLF = ((power_gen_MB / (total_asset * 2)) * 100);

                    // console.log("Grand_Power_Gen: "+grand_pow_in_MB)
                    avg_wind_direction = (wind_direction / total_asset)
                    plantsLiveData[plant.key] = {
                        powerGen: power_gen_MB.toFixed(1) ,
                        plf: PLF.toFixed(2) + "%",
                        temp: avg_temperature.toFixed(1),
                        windSpeed: avg_wind_speed.toFixed(1),
                        windDirection: avg_wind_direction.toFixed(1),
                        operational: operational,
                        waitingForWind: waitingForWind,
                        turbineDown: turbineDown,
                        emergency: emergency,
                        assetsCount: total_asset
                    }
                    // console.log(plantsLiveData)
                }


            })


            if (Object.keys(plantsLiveData).length == 0) {
                scope.setState({
                    isLoading: false,
                    dataNotFound: true
                })
            }
            scope.setState({
                plantsLiveData: plantsLiveData,
                isLoading: false
                // powerGen: power_gen_MB.toFixed(1) + "MW",
                // plf: PLF.toFixed(2)+"%",
                // temp: avg_temperature.toFixed(1),
                // windSpeed: avg_wind_speed.toFixed(1),
                // windDirection:  avg_wind_direction.toFixed(1),
                // isLoading: false,
                // operational: operational,
                // waitingForWind: waitingForWind,
                // turbineDown: turbineDown,
                // emergency: emergency
            })
            // })
        }, function (errorObject) {
            // console.log("The read failed: " + errorObject.code);
        });

        if (status == 1) {
            // console.log('inside off')
            scope.ref.off()
            alert('Live Data Stopped Successfully')

        } else if (status == 0) {
            alert('Live Data Started Successfully')
        }

    }

    shouldComponentUpdate() {
        return true
    }

    componentWillUnmount() {
        this.ref.off()
    }

    componentDidMount() {
        // console.log('in componentDidMount')
        if (obj.isConnected)
            this.getPlants()
    }


    renderTableHead() {
        let scope = this
        return (
            <View style={{
                flex: 1,
                alignSelf: 'stretch',
                flexDirection: 'row',
                backgroundColor: '#007bff',
                borderBottomWidth: 1,
                borderBottomColor: 'white'
            }}>
                <View style={[styles.tableHead, {
                    width: width / 14,
                    marginTop: 5,
                    paddingLeft: 15,
                    alignItems: 'center'
                }]}>
                    <Text style={{color: 'white'}}>#</Text>
                </View>
                <View style={[styles.tableHead, {marginLeft: width / 30}]}>
                    <Text style={{color: 'white'}}>Plant</Text>

                </View>
                <TouchableOpacity
                    // onPress={
                    //     () => scope.sortByTag('power')
                    // }
                    style={[styles.tableHead, {width: width / 3.5, marginLeft: width / 20}]}>
                    <Text style={{color: 'white'}}>Installed Capacity (MW)</Text>
                    {
                        scope.state.sortBy == 'power' &&
                        <Image style={[styles.sortIcon, {marginLeft: -10}]}
                               source={scope.state.sortIcon}/>
                    }
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.tableHead, {width: width / 3, marginLeft: width / 20}]}
                    // onPress={
                    //     () => scope.sortByTag('windSpeed')
                    // }
                >
                    <Text style={{color: 'white'}}>Instantaneous Generation (MW)</Text>
                    {
                        scope.state.sortBy == 'windSpeed' &&
                        <Image style={[styles.sortIcon, {marginLeft: -10}]}
                               source={scope.state.sortIcon}/>
                    }
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.tableHead, {marginLeft: width / 20}]}
                    // onPress={
                    //     () => scope.sortByTag('UNITS')
                    // }
                >
                    <Text style={{color: 'white'}}>Wind Speed</Text>
                    {
                        scope.state.sortBy == 'UNITS' &&
                        <Image style={styles.sortIcon}
                               source={scope.state.sortIcon}/>
                    }
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.tableHead, {marginLeft: width / 20, width: width / 3}]}
                    // onPress={
                    //     () => scope.sortByTag('TEMP')
                    // }
                >
                    <Text style={{color: 'white'}}>Actual Generation (MWh)</Text>
                    {
                        scope.state.sortBy == 'TEMP' &&
                        <Image style={styles.sortIcon}
                               source={scope.state.sortIcon}/>
                    }
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.tableHead, {marginLeft: width / 20}]}
                    // onPress={
                    //     () => scope.sortByTag('TEMP')
                    // }
                >
                    <Text style={{color: 'white', width: width / 3}}>Yesterday Generation (MWh)</Text>
                    {
                        scope.state.sortBy == 'TEMP' &&
                        <Image style={styles.sortIcon}
                               source={scope.state.sortIcon}/>
                    }
                </TouchableOpacity>

                <TouchableOpacity
                    style={[styles.tableHead, {marginLeft: width / 3, width: width / 1.5}]}
                    // onPress={
                    //     () => scope.sortByTag('TEMP')
                    // }
                >
                    <Text style={{color: 'white'}}>Operation Status</Text>
                    {
                        scope.state.sortBy == 'TEMP' &&
                        <Image style={styles.sortIcon}
                               source={scope.state.sortIcon}/>
                    }
                </TouchableOpacity>
            </View>

        );

    }


    render() {

        if (this.state.isLoading) {
            return (
                <View style={commonStyles.horizontal}>
                    <ActivityIndicator size="large" color="#0000ff"/>
                </View>
            )
        }

        let plantsLength = Object.keys(this.state.plantsLiveData).length


        return (
            <View style={commonStyles.container}>
                <OfflineNotice/>


                <ScrollView contentContainerStyle={{padding: 30}} horizontal={true}>
                    <View style={{height: plantsLength * 80, borderWidth: 1, borderColor: 'white'}}>
                        <View style={{height: height / 15}}>
                            {this.renderTableHead()}
                        </View>
                        {/*<View style={{height: height / 15}}>*/}
                        {/*    {this.renderSecondRow()}*/}
                        {/*</View>*/}
                        {
                            Object.keys(this.state.plantsLiveData).map((key, index) => {
                                return <TableRow key = {index} plant = {key} index = {index} data = {this.state.plantsLiveData} />
                            })
                        }
                    </View>
                </ScrollView>
            </View>
        )
    }
}

export default Portfolio


