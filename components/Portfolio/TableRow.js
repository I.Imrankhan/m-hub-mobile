import React, {Component} from 'react'
import {
    View,
    Text,
    Dimensions,
} from 'react-native'
import commonStyles from '../stylesCss'
import styles from "./PortfolioCss";
import {Actions} from "react-native-router-flux";
import firebase from "firebase";

let {height, width} = Dimensions.get('window')

export default class TableRow extends Component {

    fetchGenerationData = () => {
        console.log('in fetchGeneration')
        let scope = this
        var config = {
            apiKey: "AIzaSyDI4so_sVXzjAjFMLWRy8nVuH6HDtageNE",
            authDomain: "170271322381-eqbedbns3pi77vhg5c44gidh6kks0dns.apps.googleusercontent.com",
            databaseURL: "https://energon-fb-production.firebaseio.com",
            projectId: "energon-fb-production",
            storageBucket: "energon-fb-production.appspot.com",
            // messagingSenderId: "*********"
        }
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
        let db = firebase.database();
        scope.ref = db.ref("reports/fatanpur/2019-05-16/generation/");
        let listener = scope.ref.on("value", async function (snapshot) {
            console.log('inside ref')
            console.log(snapshot.val())
            snapshot.forEach((asset) => {
                console.log(asset.val()['data']['gen,potencia']['energy'])
            })
        }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
        });
    }

    componentDidMount() {
        this.fetchGenerationData()
    }

    renderSecondRow(asset, i) {
        let scope = this
        let state = this.state
        // let length = this.state.assetsData.length

        return (
            <View
                key={i}
                style={{
                    flex: 1,
                    alignSelf: 'stretch',
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    borderBottomColor: 'white'
                }}>
                {/*<View style = {{   borderLeftColor: 'white',*/}
                {/*    borderLeftWidth: 1}}/>*/}
                <View/>
                <View/>
                <View/>
                <View/>
                <View/>
                <View/>

                <View style={{
                    marginLeft: width * 2.2,
                    width: width / 5
                }}>
                    <Text style={[styles.text]}>Run (green)</Text>
                </View>
                <View>
                    <Text style={[styles.text, {width: width / 4}]}>waiting for wind (yellow)</Text>
                </View>
                <View>
                    <Text style={[styles.text, {width: width / 5, marginLeft: 10, marginRight: 10}]}>Stop (red)</Text>
                </View>
                <View>
                    <Text style={[styles.text, {width: width / 3}]}>Communication error (black)</Text>
                </View>
            </View>

        )
    }

    render() {
        let i = this.props.index
        let plant = this.props.plant
        return (
            <View
                key={i}
                style={[styles.row, {borderWidth: i == 5 ? 0 : 1, borderColor: 'white'}]}>
                <View style={{width: width / 20, paddingLeft: 10}}>
                    <Text style={styles.text}>{i + 1}</Text>
                </View>
                <View style={{}}>
                    <Text style={[styles.text, {marginLeft: width / 12, width: width / 4.7}]}>
                        {plant.toUpperCase()}
                    </Text>
                </View>
                <View style={{}}>
                    <Text style={[styles.text, {marginLeft: width / 10, width: width / 8}]}>
                        {this.props.data[plant].assetsCount * 2}
                    </Text>
                </View>
                <View style={{}}>
                    <Text style={[styles.text, {marginLeft: width / 3.5, width: width / 5}]}>
                        {this.props.data[plant]['powerGen']}
                    </Text>
                </View>

                <View style={{}}>
                    <Text style={[styles.text, {marginLeft: width / 5}]}>
                        {this.props.data[plant]['windSpeed']}
                    </Text>
                </View>
                <View style={{}}>
                    <Text style={[styles.text, {marginLeft: width / 4.5, width: width / 6}]}>1000.00</Text>
                </View>
                <View style={{}}>
                    <Text style={[styles.text, {marginLeft: width / 4.5, width: width / 6}]}>1000.00</Text>
                </View>

                <View style={[styles.opeStatus,
                    {backgroundColor: '#55cab3', marginLeft: width / 4.5,
                    }]}>
                    <Text style={[styles.text, styles.colorText]}>
                        {this.props.data[plant].operational}
                    </Text>
                </View>

                <View style={[styles.opeStatus,
                    {backgroundColor: '#FF9800',
                    }]}>
                    <Text style={[styles.text, styles.colorText]}>
                        {this.props.data[plant].waitingForWind}
                    </Text>
                </View>

                <View style={[styles.opeStatus,
                    {backgroundColor: '#F44336',
                    }]}>
                    <Text style={[styles.text, styles.colorText]}>
                        {this.props.data[plant].turbineDown}
                    </Text>
                </View>

                <View style={[styles.opeStatus,
                    {backgroundColor: '#1a2a3a', marginRight: 20
                    }]}>
                    <Text style={[styles.text, styles.colorText]}>
                        {this.props.data[plant].emergency}
                    </Text>
                </View>

            </View>
        )
    }

}