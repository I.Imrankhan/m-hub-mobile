import { StyleSheet, Dimensions } from 'react-native'
const {width, height} = Dimensions.get('window')

export default StyleSheet.create({
    text: {
        fontSize: 15,
        flex: 1,
        flexWrap: 'wrap',
        marginTop: height / 65,

    },

    tableHead: {
        flex: 1,
        alignSelf: 'stretch',
        flexDirection: 'row',
        paddingLeft: 15,
        alignItems: 'center',
        // borderRightWidth: 1 ,
        // borderRightColor: 'gray',
        color: 'white'
    },
    tableRow: {
        // flex: 1,
        // alignSelf: 'stretch',
        // padding: 7,
        // marginLeft: -15,
        // alignItems: 'center',
        // borderBottomWidth: 1 ,
        // borderBottomColor: 'white'
    },
    sortIcon: {
        width: 20,
        height: 20
    },
    row: {
        flex: 1,
        // alignSelf: 'stretch',
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: 'white',
        paddingTop: 5
    },
    colorText: {
        color: 'white'
    },
    opeStatus: {
        marginLeft: width / 6,
        width: width / 9,
        height: height / 16,
        borderRadius: 10,
        alignItems: 'center',
        opacity: 0.7
    }
})
