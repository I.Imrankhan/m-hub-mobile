import React, { Component } from 'react'

import {
  View,
  Text,
  TouchableHighlight,
  Modal,
  StyleSheet,
  Button,
  CameraRoll,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native'

//import Share from 'react-native-share'
//import RNFetchBlob from 'react-native-fetch-blob'

let styles
const { width } = Dimensions.get('window')

class ImageTaker extends Component {
    _handleButtonPress = () => {
       CameraRoll.getPhotos({
           first: 20,
           assetType: 'Photos',
         })
         .then(r => {
           this.setState({ photos: r.edges });
         })
         .catch((err) => {
            //Error Loading Images
         });
       }
    render() {
     return (
       <View>
         <Button title="Load Images" onPress={this._handleButtonPress} />
         <ScrollView>
           {this.state.photos.map((p, i) => {
           return (
             <Image
               key={i}
               style={{
                 width: 300,
                 height: 100,
               }}
               source={{ uri: p.node.image.uri }}
             />
           );
         })}
         </ScrollView>
       </View>
     );
    }
}

export default ImageTaker