// var express = require('express');
var firebase = require("firebase-admin");

var serviceAccount = require("./config.json");

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "https://energon-fb-production.firebaseio.com"
});


let db = firebase.database();

var ref = db.ref("realtime/jmd");
// console.log(ref)

function getPlants() {
  ref.on("value", function (snapshot) {
    let power_gen = 0;
    let temperature = 0;
    let asset_count = 0;
    let speed_asset_count = 0;
    let avg_wind_speed = 0;
    let wspeed = 0;
    snapshot.forEach((item) => {
      power_gen2 = +(item.val()['Gen,Potencia']['tag_value']);
      temp2 = +(item.val()['Amb,Temp']['tag_value']);
      wspeed2 = +(item.val()['Amb,VelViento']['tag_value']);

      //console.log("Raw temp " + wind_speed2);
      if (power_gen2 > 0)
        power_gen += power_gen2;

      if (temp2 > 0) {
        temperature = temperature + temp2;

        asset_count++;

      }

      if (power_gen2 > 0) {
        wspeed += wspeed2;
        speed_asset_count++;
      }
      ;


    })
    // console.log("Total temp " + temperature);
    avg_wind_speed = temperature / asset_count;
    let avg_wspeed = wspeed / speed_asset_count;

    // console.log("Power Gen " + power_gen / 1000 + "MW");
    // console.log("Temp " + avg_wind_speed);
    // console.log("Wind Speed " + avg_wspeed);
    // console.log("________");
  }, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
  });
}


module.exports = { getPlants }
