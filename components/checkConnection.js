import { NetInfo } from 'react-native'

let connectionStatus;
export default checkConnection = async() => {
    await NetInfo.isConnected.fetch().done((isConnected) => {
        if(isConnected == true)
            connectionStatus = true
        else
            connectionStatus = false
    });
    alert(connectionStatus)
    return connectionStatus
}