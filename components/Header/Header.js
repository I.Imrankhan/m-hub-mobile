import React, { Component } from 'react'
import {View, Text, StyleSheet, Dimensions} from 'react-native'
import Image from 'react-native-scalable-image';
import styles from './HeaderCss.js'
let {height, width} = Dimensions.get('window');

class Header extends Component {
//             <Image style={styles.headerImg} source = {require('../../assets/vena-logo.png')}/>

   render() {
      return (
         <View>
             <Text style = {styles.heading}>{this.props.text}</Text>
             <Image
                 width = {width / 1.7}
                 style = {styles.headerImg}
                 source = {require('../../assets/vena-logo.png')}
             />
         </View>
      )
   }
}
export default Header
