import { StyleSheet, Dimensions } from 'react-native'

let {height, width} = Dimensions.get('window');
export default StyleSheet.create({
   heading: {
        color: '#4cd137',
        fontSize: 25,
        textAlign: 'center',
   },
   headerImg: {
        marginTop: 30,
        // width: width * .6,
        // height: height / 7.2,
        marginLeft: 'auto',
        marginRight: 'auto',
   },
})