import React, { Component } from 'react'
import { View, ScrollView, ActivityIndicator, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native'
import obj from '../../components/config.js'
import styles from './ListCss.js'
import commonStyles from '../stylesCss.js'
import NoData from '../NoDatafound/NoData.js'
import OfflineNotice from '../OfflineNotice/OfflineNotice.js'

class List extends Component {

   state =  {
        name: '',
        data: [],
        dataNotFound: false,
        isLoading: true,
        searchText: ''
   }

   componentDidMount() {
         data = []
         this.props.navigation.setParams({
               title: this.props.list
          })
         fetch(obj.BASE_URL+"api/controlCenter/"+this.props.url)
            .then((response) => response.json())
            .then((responseJson) => {
                if(!responseJson.data.length) {
                    this.setState({
                        dataNotFound: true,
                        isLoading: false
                    })
                }

                else {
                    this.setState({
                        data: responseJson.data,
                        copy: responseJson.data,
                        isLoading: false
                    })
                }

            })
            .catch((error) => {
                console.error(error);
            });
   }

   handleSearch = (text) => {
        this.setState({
            searchText: text
        })

        this.setState({
            data: this.state.copy.filter(
                item => item.tname && item.tname.slice(0, text.length).toUpperCase() === text.toUpperCase()
            ),
            isLoading: false
        })
   }

   render() {
      const state = this.state
      let obj_keys = []
      let titles = []
      if(this.props.list == 'Tools') {
        obj_keys = ['tname', 'wname']
        titles = ['Tool Name', 'Warehouse']
      }

      else {
        obj_keys = ['name', 'wname', 'mcount']
        titles = ['Tool Name', 'Warehouse', 'Count']

      }

      if(state.dataNotFound) {
        return(
            <NoData />
        )
      }
      if(state.isLoading) {
        return (
            <View style = {styles.horizontal}>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>
        )
      }
      return (
        <View>
            <OfflineNotice />
            <ScrollView>
                <View style={[commonStyles.container, {marginTop: 40}]}>
                {
//                    <TextInput style = {styles.input}
//                        underlineColorAndroid = "transparent"
//                        placeholder = "Search..."
//                        autoCapitalize = "none"
//                        onChangeText = {this.handleSearch}
//                    />
//                    {
//                        this.state.searchText.length > 0 &&
//                        <Text style = {{marginBottom: 20}}>Search Results: {this.state.data.length}</Text>
//                    }
                }
                    {
                        state.data.map((item,index) =>(
                            <TouchableOpacity
                                style = {commonStyles.card}
                                key = {index}

                            >
                                {
                                    obj_keys.map((key, row_index) => (
                                        <View
                                            key = {row_index}
                                            style = {[commonStyles.row, {width: 280}]}
                                        >
                                            <Text style = {[commonStyles.title, {width : 110}]}>{titles[row_index]}</Text>
                                            <Text style = {commonStyles.text}>{item[key]}</Text>

                                         </View>
                                    ))
                                }
                            </TouchableOpacity>
                        ))
                    }
                </View>
            </ScrollView>
        </View>
      )
   }
}
export default List

