import { StyleSheet, Dimensions } from 'react-native'

import OfflineNotice from '../OfflineNotice/OfflineNotice.js'
const {width, height} = Dimensions.get('window')

export default StyleSheet.create ({

   text: {
      color: '#4f603c',
   },

     head :{
           marginLeft: 20,
           padding: 10,
           flex: 1,
           flexDirection: 'row',
           justifyContent: 'space-around'
      },

   row: {
//    textAlign: 'left',
    padding: 5,
    marginTop: 3,
    backgroundColor: '#d9f9b1',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
   },

headText: {
        fontSize: 18
   },

   name: {
        width: 160,

   },

   count: {
        textAlign: 'left'
   },

   horizontal: {
       position:  'absolute',
       top: '43%',
       left: '45%'
     },

     container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
       head: { height: 40, backgroundColor: '#f1f8ff' },
       text: { margin: 6 },


  input: {
        width: width / 1.4,
        marginBottom: 15,
        padding: 10,
        borderColor : '#aeb7bf',
        borderWidth: 1,
        borderRadius: 3
  },
})
