import React, { Component } from 'react'
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    Dimensions,
    Picker,
    Animated,
    Linking,
    Easing
} from 'react-native'
import { Actions } from 'react-native-router-flux';
import styles from './PlantsCss.js'
import obj from '../config.js'
import Plant from './Plants'

var names = {}
var spin
rotateBlade = () => {
    spinValue = new Animated.Value(0)

    Animated.loop(
        Animated.timing(
            spinValue,
            {
                toValue: 1,
                duration: 2000,
                easing: Easing.linear
            })
    ).start();

    spin = this.spinValue.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
    })
}

// rotateBlade()

makeCall = (plant, users, name) => {
    if (!names[plant])
        names[plant] = name
    let number = '';
    users.map((item) => {
        if (item.fname == names[plant])
            number = item.mobile
    })

    Linking.openURL(`tel:${number}`)
}

setName = (plant, name) => {
    names[plant] = name
}

goToAssets = () => {
    
}


export default function Card(props) {
    if (window.call != 1)
        rotateBlade()
    window.call = 1

    let showCallOption = false
    let plant = props.plant

    if (props.users) {
        if (props.users.length)
            showCallOption = true
    }
    return (
        <View style={[styles.card, { padding: 0 }]}>
            <TouchableOpacity
                onPress={() => {
                    this.goToAssets(
                    )
                }}
            >

                <View style={[styles.flexBox, {
                    backgroundColor: "#176494",
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    borderTopLeftRadius: 10,
                    borderTopRightRadius: 10
                }]}>

                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ marginTop: -2, fontWeight: 'bold', fontSize: 18, marginRight: 10, color: 'white' }}>
                            {plant.toUpperCase()}
                        </Text>
                        <Text style={{ color: 'white', marginTop: 3 }}>
                            ({props.data.assetsCount} * 2 = {props.data.assetsCount * 2} MW)
                            </Text>
                    </View>

                    {/* <Animated.Image
                        style={{
                            transform: [{ rotate: spin }],
                            width: 25, height: 25, position: 'absolute',    
                            left: '95%', top: 0
                        }}
                        source={require('../../assets/blade.png')} />
                    <Image style={{ width: 10, height: 20, marginRight: 8 }} source={require('../../assets/stand.png')} /> */}
                </View>

                <View style={[styles.flexBox, { padding: 0, flexDirection: 'row' }]}>
                    <Text style={{
                        fontWeight: 'bold', marginRight: 10
                    }}>{props.data.powerGen}
                    </Text>
                    <Text>(PLF = {props.data.plf})</Text>
                </View>

                <View style={[styles.plantDetails, { borderBottomWidth: 0 }]}>
                    <View style={[styles.detailsColumn, { width: '50%' }]}>
                        <Text style={{ fontWeight: 'bold' }}>Yest Generation</Text>
                        <Text>{props.yesGenData} MWh</Text>
                    </View>

                    <View style={[styles.detailsColumn, { width: '50%', borderRightWidth: 0 }]}>
                        <Text style={{ fontWeight: 'bold' }}>Today's Generation</Text>
                        {
                            plant.toUpperCase() == 'MANGOLI1' &&
                            <Text>-</Text>
                        }

                        {
                            plant.toUpperCase() != 'MANGOLI1' &&
                            <Text>{props.data.todayGen} MWh</Text>
                        }
                    </View>

                </View>

                <View style={styles.plantDetails}>
                    <View style={styles.detailsColumn}>
                        <Image style={styles.valueIcon}
                            source={require('../../assets/temperature.png')} />
                        <Text>{props.data.temp}°C</Text>
                    </View>
                    <View style={styles.detailsColumn}>
                        <Image style={styles.valueIcon}
                            source={require('../../assets/wind.png')} />
                        <Text>{props.data.windSpeed}m/s</Text>
                    </View>
                    <View style={[styles.detailsColumn, { borderRightWidth: 0 }]}>
                        <Image style={styles.valueIcon}
                            source={require('../../assets/direction.png')} />
                        <Text>{props.data.windDirection}</Text>
                    </View>
                </View>

                <View style={[styles.plantDetails, styles.values, { padding: 10 }]}>
                    <View
                        style={[styles.detailsColumn, styles.colorContainer, { backgroundColor: '#55cab3' }]}>
                        <Text
                            style={{ color: 'white' }}>{props.data.operational}</Text>
                    </View>
                    <View
                        style={[styles.detailsColumn, styles.colorContainer, { backgroundColor: '#FF9800' }]}>
                        <Text
                            style={{ color: 'white' }}>{props.data.waitingForWind}</Text>
                    </View>
                    <View
                        style={[styles.detailsColumn, styles.colorContainer, { backgroundColor: '#F44336' }]}>
                        <Text
                            style={{ color: 'white' }}>{props.data.turbineDown}</Text>
                    </View>
                    <View
                        style={[styles.detailsColumn, styles.colorContainer, { backgroundColor: '#1a2a3a' }]}>
                        <Text
                            style={{ color: 'white' }}>{props.data.emergency}</Text>
                    </View>
                </View>

                <View style={[styles.plantDetails, { borderBottomWidth: 0, padding: 5 }]}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontWeight: 'bold', marginRight: 10 }}>Last Updated On: </Text>
                        <Text>{props.data.updated_at}</Text>
                    </View>
                </View>
            </TouchableOpacity>


            {
                showCallOption &&

                <View style={[styles.plantDetails, { padding: 10, alignItems: 'center' }]}>
                    <Text style={{ marginRight: 5, fontWeight: 'bold', fontSize: 16 }}>
                        SELECT
                        </Text>
                    <View style={{ borderWidth: 1, borderRadius: 5, borderColor: '#AFAAAA', width: '60%', height: 40 }}>
                        <Picker
                            style={{ marginTop: -8 }}
                            selectedValue={names[plant]}
                            onValueChange={(itemValue) =>
                                this.setName(plant, itemValue)
                            }>
                            {
                                props.users.map((item, index) => (

                                    < Picker.Item
                                        key={index}
                                        label={item.fname}
                                        value={item.fname}
                                    />
                                ))
                            }
                        </Picker>
                    </View>
                    <TouchableOpacity
                        onPress={() => this.makeCall(plant, props.users, props.name)}
                    >
                        <Image style={{ width: 40, height: 40 }}
                            source={require('../../assets/call.png')} />
                    </TouchableOpacity>
                </View>
            }
        </View>
    )
}