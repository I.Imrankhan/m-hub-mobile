import { Dimensions, StyleSheet } from 'react-native'

let { height, width } = Dimensions.get('window')

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        marginTop: 60,
        paddingBottom: 50
    },
    card: {
        padding: 10,
        marginBottom: 40,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 10, height: 10 },
        shadowOpacity: .7,
        shadowRadius: 2,
        elevation: 5,
        width: '80%'
    },
    row: {
        width: 300,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    text: {
        padding: 10,
        width: 130,
    },

    count: {
        marginLeft: 10,
        fontSize: 18,
        fontWeight: '400'

    },
    icon: {

        width: 20,
        height: 20
    },

    valueIcon: {
        width: 30,
        height: 30
    },

    plantName: {
        paddingLeft: 10,
        paddingBottom: 10,
        fontSize: 18,
        fontWeight: 'bold'
    },
    horizontal: {
        position: 'absolute',
        top: '43%',
        left: '45%'
    },
    flexBox: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
        padding: 10,
    },
    plantDetails: {
        borderColor: 'lightgray',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        // padding: 10,
        // marginBottom: 10
    },
    detailsColumn: {
        alignItems: 'center',
        borderRightWidth: 1,
        borderColor: 'lightgray',
        padding: 5,
        width: '33%'
    },
    colorContainer: {
        backgroundColor: 'green',
        borderRadius: 10,
        width: width / 9,
        height: height / 16,
        paddingTop: height / 60,
        alignItems: 'center',
        opacity: 0.7,
        // borderWidth: 1,
    },
    values: {
        borderTopWidth: 0,
        borderBottomWidth: 0,
        paddingRight: 30,
        paddingLeft: 30,
        margin: 0
    },
    submitButton: {
        backgroundColor: '#176494',
        padding: 10,
        textAlign: 'center',
        alignItems: 'center',
    },

    lastUpdated: {
        position: 'absolute',
        left: '68%',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5
    },

    round: {
        width: 10,
        height: 10,
        borderRadius: 50,
        backgroundColor: 'green',
        marginRight: 5
    }

})