import React, { Component } from 'react'
import {
    View,
    ScrollView,
    Image,
    Text,
    TouchableOpacity,
    Easing,
    ActivityIndicator,
    Dimensions,
    Picker,
    Linking,
    Animated
} from 'react-native'
import styles from './PlantsCss.js'
import obj from '../config.js'
import { Actions } from 'react-native-router-flux';
import NoData from '../NoDatafound/NoData.js'
import firebase from 'firebase'
import OfflineNotice from '../OfflineNotice/OfflineNotice.js'
import moment from 'moment'
import AsyncStorage from '@react-native-community/async-storage';
// import { getPlants } from '../users'
// import nodejs from 'nodejs-mobile-react-native';
import { getUsersofPlants } from '../Network/api/serviceOrdersApi'
import fConfig from './firebaseConfig'
import checkPlantdownStatus from './checkPlantStatus'
const { width, height } = Dimensions.get('window')

class Plants extends Component {

    powerGen
    plf
    temp
    windSpeed
    windDirection
    gamesaPlants = ['tgp1'
        , 'fatanpur'
        , 'patan'
        , 'tgp2'
        , 'jath'
        , 'jmd'
        , 'pililla'
    ]
    vestasPlants = ['mangoli1']
    ref
    yesRef
    state = {
        assetsData: [],
        dataNotFound: false,
        isLoading: true,
        plants: [],
        text: 'STOP',
        liveDataStatus: 1,
        yesGenData: {},
        callYesGen: 1,
        usersData: {},
    }
    spin

    img = {
        Wind: require('../../assets/wind_unselect.png'),
        Solar: require('../../assets/solar_unselect.png')
    }


    rotateBlade = () => {
        spinValue = new Animated.Value(0)

        Animated.loop(
            Animated.timing(
                spinValue,
                {
                    toValue: 1,
                    duration: 2000,
                    easing: Easing.linear
                })
        ).start();

        this.spin = spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })
    }


    goToAssets = (plant_name) => {
        let scope = this
        let yesGen = scope.state.yesGenData[plant_name]
        scope.ref.off()
        scope.yesRef.off()
        if (obj.isConnected) {
            this.setState({
                text: 'STOP'
            })
            scope.getPlantLiveData('off')
            Actions.assets({
                plant: plant_name.toLowerCase(),
                yesGen: yesGen,
                users: scope.state.usersData[plant_name]
            })
        }
    }

    getPlants = async () => {
        let plants = [];
        let plantsWithId = []
        await fetch(obj.BASE_URL + "api/controlCenter/getAssetByUserId/" + obj.user_hCode)
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.data.length) {
                    responseJson.data.map((item) => {
                        plants.push(item.plant_name.replace(/[^a-zA-Z1-9 ]/g, "").toLowerCase()),
                            plantsWithId.push({ 'plant': item.plant_name, 'id': item.site_id_fk })
                    })
                } else {
                    this.setState({
                        dataNotFound: true
                    })
                }
            })
        this.setState({
            plants: plants,
            plantsWithId: plantsWithId
        })

        this.getUsers();

        this.state.plants.map((plant, index) => {
            this.getYesterdayGeneration(plant, index)
        })
        this.getYesterdayGeneration('mangoli1')

        this.getPlantLiveData('off')

    }

    getUsers = () => {
        let usersData = {}
        let selectedName = {}
        let plantId
        this.state.plantsWithId.map((plant) => {

            getUsersofPlants(plant.id).then((data) => {
                let users = []
                // usersData[plant.plant.toLowerCase()] = data.data
                if (data.data.length) {
                    data.data.map((user) => {
                        if (user.mobile != null) {
                            users.push(user)
                            if (selectedName[plant.plant.toLowerCase()] == undefined) {
                                selectedName[plant.plant.toLowerCase()] = user.fname
                            }
                        }
                    })
                }
                usersData[plant.plant.toLowerCase()] = users
            })
        })
        this.setState({
            usersData: usersData,
            selectedName: selectedName
        })
    }


    getYesterdayGeneration = async (plant, index) => {
        // console.log('in yes fun' + plant)
        var date = new Date();
        date.setDate(date.getDate() - 1)
        let day = date.getDate()
        let month = date.getMonth()
        if (day < 10) {
            day = '0' + day
        }

        if (month < 10) {
            month = '0' + (month + 1)
        }

        date = date.getFullYear() + '-' + month + '-' + day
        let scope = this
        let yesGenData;
        let total = 0;
        let config = {
            apiKey: "AIzaSyCnuGHYZkw8eKFItnGDrLvfAySGyG0hRe4",
            authDomain: "energon-fb-production.firebaseapp.com",
            databaseURL: "https://energon-fb-production.firebaseio.com",
            projectId: "energon-fb-production",
            storageBucket: "energon-fb-production.appspot.com",
            // messagingSenderId: "*********"
        }
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
        let db = firebase.database();
        scope.yesRef = db.ref(`reports/${plant}/${date}/generation/`);
        let listener = scope.yesRef.once("value", async function (snapshot) {
            // console.log(snapshot.val())
            if (snapshot.val() != null) {
                snapshot.forEach((asset) => {

                    if (scope.gamesaPlants.includes(plant) && asset.val()['data']['gen,potencia']) {
                        total += asset.val()['data']['gen,potencia']['energy']
                    }
                    else if (scope.vestasPlants.includes(plant) && asset.val()['data']['grid,power'])
                        total += asset.val()['data']['grid,power']['energy']
                })
                // if (scope.state.plantsLiveData[plant] != undefined) {
                // console.log('' + total)
                yesGenData = scope.state.yesGenData
                yesGenData[plant] = (total / 1000).toFixed(2)
                scope.setState({
                    yesGenData: yesGenData
                })

                // }
            }

            if (index == scope.state.plants.length - 1) {
                // console.log('in if for false')
                scope.setState({
                    callYesGen: 0
                })
            }
        })

    }



    getPlantLiveData = (status) => {
        let scope = this
        let count = 0;
        if (status == 1) {
            scope.setState({
                liveDataStatus: 0,
                text: 'START'
            })
        } else if (status == 0) {
            scope.setState({
                liveDataStatus: 1,
                text: 'STOP'
            })
        }

        let plantsLiveData = {}

        if (!firebase.apps.length) {
            firebase.initializeApp(fConfig);
        }
        let db = firebase.database();
        scope.ref = db.ref("realtime/");
        scope.ref.on("value", async function (snapshot) {
            snapshot.forEach((plant) => {
                // alert(plant.key)
                if ((scope.state.plants.includes(plant.key) && (scope.gamesaPlants.includes(plant.key) || scope.vestasPlants.includes(plant.key))) || plant.key == 'mangoli1') {
                    // console.log(plant.key)
                    let tag;

                    let total_power_gen = 0;
                    let total_temperature = 0;
                    let total_wind_speed = 0;

                    let temp_asset_count = 0;
                    let speed_asset_count = 0;
                    let gen_asset_count = 0;
                    let wind_direction = 0;
                    let total_asset = 0;
                    let grand_pw_gen = 0;
                    let avg_temperature = 0;
                    let avg_wind_speed = 0;
                    let power_gen_MB = 0;
                    let grand_pow_in_MB = 0;
                    let PLF = 0;
                    let avg_wind_direction = 0;
                    let operational = 0;
                    let waitingForWind = 0;
                    let turbineDown = 0;
                    let emergency = 0;
                    let todayGen = 0;
                    plant.forEach((item) => {

                        if (scope.gamesaPlants.includes(plant.key)) {
                            todayGen += parseFloat(item.val()["Prod,DiaActual"]['tag_value'])

                            tag = "Estado"
                            power_gen2 = +(item.val()['Gen,Potencia']['tag_value']);
                            temp2 = +(item.val()['Amb,Temp']['tag_value']);
                            wspeed2 = +(item.val()['Amb,VelViento']['tag_value']);
                            wdirection2 = +(item.val()['Amb,Direccion']['tag_value']);

                            if ((new Date(item.val()[tag]['created_at']) < new Date(item.val()['ErrorComunicaciones']['created_at']))) {
                                if (item.val().ErrorComunicaciones.tag_value == 'true') {
                                    emergency++;
                                }
                            } else if ((parseInt(item.val()[tag].tag_value) == 125)) {
                                emergency++;
                            } else if (parseInt(item.val()[tag].tag_value) == 100 || item.val()[tag].tag_value == "undefined") {
                                operational++;
                            } else if (parseInt(item.val()[tag].tag_value) == 75) {
                                waitingForWind++;
                            } else if (parseInt(item.val()[tag].tag_value) == 0 || parseInt(item.val()[tag].tag_value) == 25 || parseInt(item.val()[tag].tag_value) == 50) {
                                turbineDown++;
                            }
                        } else if (scope.vestasPlants.includes(plant.key)) {
                            todayGen = "-"

                            tag = "System,OperationStateInt"
                            power_gen2 = +(item.val()['Grid,Power']['tag_value']);
                            temp2 = +(item.val()['Ambient,Temperature']['tag_value']);
                            wspeed2 = +(item.val()['Ambient,WindSpeed']['tag_value']);
                            wdirection2 = +(item.val()['Ambient,WindDir']['tag_value']);

                            if (parseInt(item.val()[tag].tag_value) == 0) {
                                emergency++;
                            } else if (parseInt(item.val()[tag].tag_value) == 3) {
                                operational++;
                            } else if (parseInt(item.val()[tag].tag_value) == 2) {
                                waitingForWind++;
                            } else if (parseInt(item.val()[tag].tag_value) == 1) {
                                turbineDown++;
                            }
                        }

                        grand_pw_gen = +power_gen2;

                        wind_direction += wdirection2;
                        if (power_gen2 > 0) {
                            total_power_gen += power_gen2;
                            gen_asset_count++;
                        }
                        if (temp2 > 0) {
                            total_temperature += temp2;
                            temp_asset_count++;
                        }
                        if (wspeed2 > 0) {
                            total_wind_speed += wspeed2;
                            speed_asset_count++;
                        }
                        total_asset++;
                    })
                    avg_temperature = total_temperature / temp_asset_count;
                    avg_wind_speed = total_wind_speed / speed_asset_count;
                    power_gen_MB = total_power_gen / 1000;
                    grand_pow_in_MB = grand_pw_gen / 1000;
                    PLF = ((power_gen_MB / (total_asset * 2)) * 100);

                    // console.log("Grand_Power_Gen: "+grand_pow_in_MB)
                    avg_wind_direction = (wind_direction / total_asset)
                    plantsLiveData[plant.key] = {
                        powerGen: power_gen_MB.toFixed(1) + " MW",
                        plf: PLF.toFixed(2) + "%",
                        temp: avg_temperature.toFixed(1),
                        windSpeed: avg_wind_speed.toFixed(1),
                        windDirection: avg_wind_direction.toFixed(1),
                        operational: operational,
                        waitingForWind: waitingForWind,
                        turbineDown: turbineDown,
                        emergency: emergency,
                        assetsCount: total_asset,
                        todayGen: (todayGen / 1000).toFixed(2),
                    }

                    count++;

                    if (scope.state.plantsData != undefined && plant.key == 'patan') {
                        // console.log('1:'); console.log(plantsLiveData[plant.key].windDirection)
                        if (checkPlantdownStatus(plantsLiveData[plant.key], scope.state, plant.key)) {

                        }
                    }
                }


            })


            if (Object.keys(plantsLiveData).length == 0) {
                scope.setState({
                    isLoading: false,
                    dataNotFound: true
                })
            }

            // if (count < 10) {
            scope.setState({
                plantsData: plantsLiveData,
                isLoading: false
            })
            // }
            // })
        }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
        });

        if (status == 1) {
            scope.ref.off()
            alert('Live Data Stopped Successfully')

        } else if (status == 0) {
            alert('Live Data Started Successfully')
        }
    }

    componentWillUnmount() {
        this.ref.off()
    }

    componentDidUpdate() {
    }

    arrays_equal = (a, b) => !!a && !!b && !(a < b || b < a)



    componentDidMount() {
        // this.rotateBlade()

        if (obj.isConnected) {
            this.getPlants()
        }
        // setTimeout(this.setValues(power_gen_MB, PLF, avg_temperature, avg_wind_speed, avg_wind_direction), 3000)

        data = {}
        assets = []
        assetsName = []
        if (obj.isConnected) {
            // fetch(obj.BASE_URL+"api/controlCenter/getAssetByUserId/"+this.props.hCode)
            // .then((response) => response.json())
            // .then((responseJson) => {
            //     if(!responseJson.data.length) {
            //         this.setState({
            //             dataNotFound: true
            //         })
            //     }
            //     responseJson.data.map((item) => (
            //         assets = [],
            //         item.assets.map((asset) =>(
            //             assets.push({id: asset.id, asset_name:asset.asset_name})
            //         )),
            //
            //         data[item.plant_name] = {
            //             assets: assets,
            //             id: item.site_id_fk,
            //             capacity: item.site_capacity,
            //             type: item.asset_type,
            //             unit: item.units
            //         }
            //     ))
            //    AsyncStorage.setItem('assets', data);
            //     this.setState({
            //         assetsData: data
            //     })
            // })
            // .catch((error) => {
            //     console.error(error);
            // });
        }
    }

    makeCall = (plantName) => {
        let plant = plantName
        let state = this.state
        let number = '';
        state.usersData[plant].map((item) => {
            if (item.fname == state.selectedName[plant])
                number = item.mobile

        })

        Linking.openURL(`tel:${number}`)
    }

    setName = (name, plant) => {
        let selectedName = this.state.selectedName
        selectedName[plant] = name
        this.setState({
            selectedName: selectedName
        })
    }

    createCard = (plant, index) => {
        let state = this.state
        let showCallOption = false

        if (state.usersData[plant]) {
            if (state.usersData[plant].length)
                showCallOption = true
        }
        return (
            <View style={[styles.card, { padding: 0 }]} key={index}>
                <TouchableOpacity
                    onPress={() => {
                        this.goToAssets(plant)
                    }}
                >

                    <View style={[styles.flexBox, {
                        backgroundColor: "#176494",
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10
                    }]}>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 18, marginRight: 10, color: 'white' }}>
                                {plant.toUpperCase()}
                            </Text>
                            <Text style={{ color: 'white', marginTop: 3 }}>
                                ({state.plantsData[plant].assetsCount} * 2
                                = {state.plantsData[plant].assetsCount * 2} MW)
                            </Text>
                        </View>

                        {/* <Animated.Image
                            style={{
                                transform: [{ rotate: this.spin }],
                                width: 25, height: 25, position: 'absolute',
                                left: '95%', top: 5, zIndex: 10
                            }}
                            source={require('../../assets/blade.png')} 
                        />
                        <Image style={{ width: 10, height: 20, marginRight: 8, marginTop: 10 }} source={require('../../assets/stand.png')} /> */}

                    </View>



                    <View style={[styles.flexBox, { padding: 0, flexDirection: 'row' }]}>
                        <Text style={{
                            fontWeight: 'bold', marginRight: 10
                        }}>{state.plantsData[plant].powerGen}
                        </Text>
                        <Text>(PLF = {state.plantsData[plant].plf})</Text>
                    </View>

                    <View style={[styles.plantDetails, { borderBottomWidth: 0 }]}>
                        <View style={[styles.detailsColumn, { width: '50%' }]}>
                            <Text style={{ fontWeight: 'bold' }}>Yest Generation</Text>
                            <Text>{state.yesGenData[plant]} MWh</Text>
                        </View>

                        <View style={[styles.detailsColumn, { width: '50%', borderRightWidth: 0 }]}>
                            <Text style={{ fontWeight: 'bold' }}>Today's Generation</Text>
                            {
                                plant.toUpperCase() == 'MANGOLI1' &&
                                <Text>-</Text>
                            }

                            {
                                plant.toUpperCase() != 'MANGOLI1' &&
                                <Text>{state.plantsData[plant].todayGen} MWh</Text>
                            }
                        </View>

                    </View>

                    <View style={styles.plantDetails}>
                        <View style={styles.detailsColumn}>
                            <Image style={styles.valueIcon}
                                source={require('../../assets/temperature.png')} />
                            <Text>{state.plantsData[plant].temp}°C</Text>
                        </View>
                        <View style={styles.detailsColumn}>
                            <Image style={styles.valueIcon}
                                source={require('../../assets/wind.png')} />
                            <Text>{state.plantsData[plant].windSpeed}m/s</Text>
                        </View>
                        <View style={[styles.detailsColumn, { borderRightWidth: 0 }]}>
                            <Image style={styles.valueIcon}
                                source={require('../../assets/direction.png')} />
                            <Text>{state.plantsData[plant].windDirection}</Text>
                        </View>
                    </View>

                    <View style={[styles.plantDetails, styles.values, { padding: 10 }]}>
                        <View
                            style={[styles.detailsColumn, styles.colorContainer, { backgroundColor: '#55cab3' }]}>
                            <Text style={{ color: 'white' }}>{state.plantsData[plant].operational}</Text>
                        </View>
                        <View
                            style={[styles.detailsColumn, styles.colorContainer, { backgroundColor: '#FF9800' }]}>
                            <Text
                                style={{ color: 'white' }}>{state.plantsData[plant].waitingForWind}</Text>
                        </View>
                        <View
                            style={[styles.detailsColumn, styles.colorContainer, { backgroundColor: '#F44336' }]}>
                            <Text
                                style={{ color: 'white' }}>{state.plantsData[plant].turbineDown}</Text>
                        </View>
                        <View
                            style={[styles.detailsColumn, styles.colorContainer, { backgroundColor: '#1a2a3a' }]}>
                            <Text
                                style={{ color: 'white' }}>{state.plantsData[plant].emergency}</Text>
                        </View>
                    </View>

                    <View style={[styles.plantDetails, { borderBottomWidth: 0, padding: 5 }]}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontWeight: 'bold', marginRight: 10 }}>Last Updated: </Text>
                            <Text>{state.plantsData[plant].updated_at}</Text>
                        </View>
                    </View>
                </TouchableOpacity>


                {
                    showCallOption &&

                    <View style={[styles.plantDetails, { padding: 10, alignItems: 'center' }]}>
                        <Text style={{ marginRight: 5, fontWeight: 'bold', fontSize: 16 }}>
                            SELECT
                        </Text>
                        <View style={{ borderWidth: 1, borderRadius: 5, borderColor: '#AFAAAA', width: '60%', height: 40 }}>
                            <Picker
                                style={{ marginTop: -8 }}
                                selectedValue={state.selectedName[plant]}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setName(itemValue, plant)
                                }>
                                {
                                    state.usersData[plant].map((item, index) => (

                                        < Picker.Item
                                            key={index}
                                            label={item.fname}
                                            value={item.fname}
                                        />
                                    ))
                                }
                            </Picker>
                        </View>
                        <TouchableOpacity
                            onPress={() => this.makeCall(plant)}
                        >
                            <Image style={{ width: 40, height: 40 }}
                                source={require('../../assets/call.png')} />
                        </TouchableOpacity>
                    </View>
                }
            </View>
        )
    }

    render() {
        let state = this.state
        if (this.state.dataNotFound) {
            return (
                <NoData />
            );
        } else if (this.state.isLoading) {
            return (
                <View style={styles.horizontal}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        }

        return (
            <View>
                <OfflineNotice />
                {
                    obj.isConnected &&
                    <TouchableOpacity
                        style={[
                            styles.submitButton,
                        ]}
                        onPress={
                            () => this.getPlantLiveData(this.state.liveDataStatus)
                        }
                    >
                        <Text style={{ color: 'white' }}>{state.text} LIVE DATA</Text>
                    </TouchableOpacity>
                }
                <ScrollView>
                    <View style={styles.container}>
                        {
                            Object.keys(state.plantsData).map((key, index) => (
                                this.createCard(key, index)
                            ))

                        }
                    </View>
                </ScrollView>
            </View>
        )
    }
}

export default Plants
