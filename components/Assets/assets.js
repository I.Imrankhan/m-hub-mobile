import React, { Component } from 'react';
import {
    View,
    ScrollView,
    Text,
    Image,
    Button,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    TextInput, Dimensions, Picker, Linking
} from 'react-native';
import obj from '../config.js'
import styles from './assetCss.js'
import plantStyles from '../Plants/PlantsCss'
import { Actions } from 'react-native-router-flux';
import OfflineNotice from '../OfflineNotice/OfflineNotice.js'
import firebase from 'firebase'
import moment from 'moment'
import Card from '../Plants/Card'

const { width, height } = Dimensions.get('window')


export default class Assets extends Component {
    ref;
    state = {
        assetsData: [],
        searchText: '',
        isLoading: true,
        sortOrder: 1,
        current: '',
        previous: '',
        text: 'STOP',
        sortBy: '',
        liveDataStatus: 1
    }

    goToCreatePunch = (name, id) => {
        if (obj.isConnected)
            Actions.createPunchPoints({ asset_id: id, plant_id: this.props.plant_id, asset_name: name })
    }


    getPlantLiveData = (status) => {
        let assets = {}
        let assetsData = []
        let scope = this

        if (status == 1) {
            scope.setState({
                liveDataStatus: 0,
                text: 'START'
            })
        } else if (status == 0) {
            scope.setState({
                liveDataStatus: 1,
                text: 'STOP'
            })
        }

        let gamesaPlants = ['tgp1'
            , 'fatanpur'
            , 'patan'
            , 'tgp2'
            , 'jath'
            , 'jmd'
            , 'pililla'
        ]
        let vestasPlants = ['mangoli1']
        let plantsLiveData = {}

        let config = {
            apiKey: "AIzaSyDI4so_sVXzjAjFMLWRy8nVuH6HDtageNE",
            authDomain: "170271322381-eqbedbns3pi77vhg5c44gidh6kks0dns.apps.googleusercontent.com",
            databaseURL: "https://energon-fb-production.firebaseio.com",
            projectId: "energon-fb-production",
            storageBucket: "energon-fb-production.appspot.com",
            // messagingSenderId: "*********"
        }
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
        let db = firebase.database();
        scope.ref = db.ref("realtime/" + scope.props.plant);
        scope.ref.on("value", async function (snapshot) {
            assetsData = []
            // console.log(snapshot.val())
            let tag;

            let total_power_gen = 0;
            let total_temperature = 0;
            let total_wind_speed = 0;

            let temp_asset_count = 0;
            let speed_asset_count = 0;
            let gen_asset_count = 0;
            let wind_direction = 0;
            let total_asset = 0;
            let grand_pw_gen = 0;
            let avg_temperature = 0;
            let avg_wind_speed = 0;
            let power_gen_MB = 0;
            let grand_pow_in_MB = 0;
            let PLF = 0;
            let avg_wind_direction = 0;
            let operational = 0;
            let waitingForWind = 0;
            let turbineDown = 0;
            let emergency = 0;
            let todayGen = 0;
            snapshot.forEach((item) => {
                let assetObj = {}
                if (gamesaPlants.includes(snapshot.key)) {
                    todayGen += parseFloat(item.val()["Prod,DiaActual"]['tag_value'])

                    tag = "Estado"
                    power_gen2 = +(item.val()['Gen,Potencia']['tag_value']);
                    temp2 = +(item.val()['Amb,Temp']['tag_value']);
                    wspeed2 = +(item.val()['Amb,VelViento']['tag_value']);
                    wdirection2 = +(item.val()['Amb,Direccion']['tag_value']);

                    if ((new Date(item.val()[tag]['created_at']) < new Date(item.val()['ErrorComunicaciones']['created_at']))) {
                        if (item.val().ErrorComunicaciones.tag_value == 'true') {
                            emergency++;
                        }
                    } else if ((parseInt(item.val()[tag].tag_value) == 125)) {
                        emergency++;
                    } else if (parseInt(item.val()[tag].tag_value) == 100 || item.val()[tag].tag_value == "undefined") {
                        operational++;
                    } else if (parseInt(item.val()[tag].tag_value) == 75) {
                        waitingForWind++;
                    } else if (parseInt(item.val()[tag].tag_value) == 0 || parseInt(item.val()[tag].tag_value) == 25 || parseInt(item.val()[tag].tag_value) == 50) {
                        turbineDown++;
                    }

                    assets[item.key] = {
                        asset: item.key
                        , power: parseFloat(item.val()["Gen,Potencia"]['tag_value']).toFixed(2)
                        , windSpeed: parseFloat(item.val()["Amb,VelViento"]['tag_value']).toFixed(2)
                        , RPM: parseFloat(item.val()["Rotor,Vel"]['tag_value']).toFixed(2)
                        , DIR: parseFloat(item.val()["Amb,Direccion"]['tag_value']).toFixed(2)
                        , UNITS: parseFloat(item.val()["Prod,DiaActual"]['tag_value']).toFixed(0)
                        , PITCH: parseFloat(item.val()["Rotor,AnguloPitch"]['tag_value']).toFixed(2)
                        , TEMP: parseFloat(item.val()["Amb,Temp"]['tag_value']).toFixed(2)
                    }


                    assetsData.push(assets[item.key])


                } else if (vestasPlants.includes(snapshot.key)) {
                    todayGen = "-"
                    tag = "System,OperationStateInt"
                    power_gen2 = +(item.val()['Grid,Power']['tag_value']);
                    temp2 = +(item.val()['Ambient,Temperature']['tag_value']);
                    wspeed2 = +(item.val()['Ambient,WindSpeed']['tag_value']);
                    wdirection2 = +(item.val()['Ambient,WindDir']['tag_value']);

                    if (parseInt(item.val()[tag].tag_value) == 0) {
                        emergency++;
                    } else if (parseInt(item.val()[tag].tag_value) == 3) {
                        operational++;
                    } else if (parseInt(item.val()[tag].tag_value) == 2) {
                        waitingForWind++;
                    } else if (parseInt(item.val()[tag].tag_value) == 1) {
                        turbineDown++;
                    }
                    assets[item.key] = {
                        asset: item.key
                        , power: parseFloat(item.val()["Grid,Power"]['tag_value']).toFixed(2)
                        , windSpeed: parseFloat(item.val()["Ambient,WindSpeed"]['tag_value']).toFixed(2)
                        , RPM: parseFloat(item.val()["RotorSystem,RotorRPM"]['tag_value']).toFixed(2)
                        , DIR: parseFloat(item.val()["Ambient,WindDir"]['tag_value']).toFixed(2)
                        , UNITS: "-"
                        , TEMP: parseFloat(item.val()["Ambient,Temperature"]['tag_value']).toFixed(2)
                    }
                    assetsData.push(assets[item.key])

                }


                grand_pw_gen = +power_gen2;

                wind_direction += wdirection2;
                if (power_gen2 > 0) {
                    total_power_gen += power_gen2;
                    gen_asset_count++;
                }
                if (temp2 > 0) {
                    total_temperature += temp2;
                    temp_asset_count++;
                }
                if (wspeed2 > 0) {
                    total_wind_speed += wspeed2;
                    speed_asset_count++;
                }
                total_asset++;
            })
            avg_temperature = total_temperature / temp_asset_count;
            avg_wind_speed = total_wind_speed / speed_asset_count;
            power_gen_MB = total_power_gen / 1000;
            grand_pow_in_MB = grand_pw_gen / 1000;
            PLF = ((power_gen_MB / (total_asset * 2)) * 100);

            // console.log("Grand_Power_Gen: "+grand_pow_in_MB)
            avg_wind_direction = (wind_direction / total_asset)
            plantsLiveData[scope.props.plant] = {
                powerGen: power_gen_MB.toFixed(1) + " MW",
                plf: PLF.toFixed(2) + "%",
                temp: avg_temperature.toFixed(1),
                windSpeed: avg_wind_speed.toFixed(1),
                windDirection: avg_wind_direction.toFixed(1),
                operational: operational,
                waitingForWind: waitingForWind,
                turbineDown: turbineDown,
                emergency: emergency,
                todayGen: (todayGen / 1000).toFixed(2),
                assetsCount: total_asset,
                updated_at: moment(new Date()).format('DD-MM-YYYY   hh:mm:ss a')
            }
            // console.log(plantsLiveData)

            scope.setState({
                plantsLiveData: plantsLiveData,
                isLoading: false,
                assets: assets,
                assetsData: assetsData
            })
            // })
        }, function (errorObject) {
            // console.log("The read failed: " + errorObject.code);
        });

        if (status == 1) {
            // console.log('inside off')
            scope.ref.off()
            // alert('Live Data Stopped Successfully')

        } else if (status == 0) {
            alert('Live Data Started Successfully')
        }

    }

    shouldComponentUpdate() {
        return true
    }

    componentWillUnmount() {
        this.ref.off()
    }

    componentDidMount() {
        if (this.props.users) {
            if (this.props.users.length) {
                this.setState({
                    selectedName: this.props.users[0].fname
                })
            }
        }
        // console.log('in componentDidMount')
        if (obj.isConnected)
            this.getPlantLiveData()
        this.props.navigation.setParams({
            title: 'Assets of ' + this.props.plant.charAt(0).toUpperCase() + this.props.plant.slice(1)
        })
        // data = {}
        // arr = []
        // assets = []
        // assetsName = []
        // this.setState({
        //     assetsData: this.props.assets
        // })
    }


    // handleSearch = (text) => {
    //     this.setState({
    //         searchText: text
    //     })
    //     this.setState({
    //         assetsData: this.props.assets.filter(
    //                 asset => asset.asset_name && asset.asset_name.slice(0, text.length).toUpperCase() === text.toUpperCase()
    //         )
    //     })
    // }


    //    searchAssets = (asset)  => {
    //        console.log(this.state.searchText)
    //        if(!this.state.searchText.length) {
    //            this.setState({
    //                assetsData: this.props.assets
    //            })
    //        }
    //        if(asset.asset_name)
    //            return asset.asset_name.slice(0, this.state.searchText.length) === this.state.searchText
    //
    //
    //    }

    makeCall = () => {
        let state = this.state
        let number = '';
        this.props.users.map((item) => {
            if (item.fname == state.selectedName)
                number = item.mobile

        })
        // console.log('calling to:' + number)
        Linking.openURL(`tel:${number}`)
    }

    setName = (name) => {
        this.setState({
            selectedName: name
        })
    }

    sortByTag = async (tag) => {
        if (tag != this.state.previous) {
            this.setState({
                sortOrder: await 1,
            })
        }
        this.setState({
            previous: await tag,
            sortBy: await tag
        })

        if (this.state.sortOrder == 1) {
            this.setState({
                sortIcon: require('../../assets/asec-sort.png'),
                assetsData: this.state.assetsData.sort(this.sortByAscending),
                sortOrder: 2
            })
        } else {
            this.setState({
                sortIcon: require('../../assets/desc-sort.png'),
                assetsData: this.state.assetsData.sort(this.sortByDescending),
                sortOrder: 1
            })
        }

    }

    sortByAscending = (a, b) => {
        if (parseFloat(a[this.state.sortBy]) < parseFloat(b[this.state.sortBy]))
            return -1;
        if (parseFloat(a[this.state.sortBy]) > parseFloat(b[this.state.sortBy]))
            return 1;
        return 0;
    }

    sortByDescending = (a, b) => {
        if (parseFloat(a[this.state.sortBy]) < parseFloat(b[this.state.sortBy]))
            return 1;
        if (parseFloat(a[this.state.sortBy]) > parseFloat(b[this.state.sortBy]))
            return -1;
        return 0;
    }

    renderTableHead() {
        let scope = this
        return (
            <View style={{
                flex: 1,
                alignSelf: 'stretch',
                flexDirection: 'row',
                backgroundColor: '#007bff',
                borderBottomWidth: 1,
                borderBottomColor: 'white'
            }}>
                <View style={{ width: width / 14, marginTop: 5, paddingLeft: 15, alignItems: 'center' }}>
                    <Text style={{ color: 'white' }}>#</Text>
                </View>
                <View style={[styles.tableHead, { marginLeft: width / 60 }]}>
                    <Text style={{ color: 'white' }}>Asset</Text>

                </View>
                <TouchableOpacity
                    onPress={
                        () => scope.sortByTag('power')
                    }
                    style={[styles.tableHead]}>
                    <Text style={{ color: 'white' }}>Power (KW)</Text>
                    {
                        scope.state.sortBy == 'power' &&
                        <Image style={[styles.sortIcon, { marginLeft: -10 }]}
                            source={scope.state.sortIcon} />
                    }
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={
                        () => scope.sortByTag('windSpeed')
                    }
                    style={styles.tableHead}>
                    <Text style={{ color: 'white' }}>Wind (M/S)</Text>
                    {
                        scope.state.sortBy == 'windSpeed' &&
                        <Image style={[styles.sortIcon, { marginLeft: -10 }]}
                            source={scope.state.sortIcon} />
                    }
                </TouchableOpacity>
                {/*<View style={styles.tableText}>*/}
                {/*    <Text>RPM</Text>*/}
                {/*</View>*/}
                {/*<View style={styles.tableText}>*/}
                {/*    <Text>DIR</Text>*/}
                {/*</View>*/}
                <TouchableOpacity
                    onPress={
                        () => scope.sortByTag('UNITS')
                    }
                    style={styles.tableHead}>
                    <Text style={{ color: 'white' }}>Units</Text>
                    {
                        scope.state.sortBy == 'UNITS' &&
                        <Image style={styles.sortIcon}
                            source={scope.state.sortIcon} />
                    }
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={
                        () => scope.sortByTag('TEMP')
                    }
                    style={styles.tableHead}>
                    <Text style={{ color: 'white' }}>Temp</Text>
                    {
                        scope.state.sortBy == 'TEMP' &&
                        <Image style={styles.sortIcon}
                            source={scope.state.sortIcon} />
                    }
                </TouchableOpacity>
            </View>
        );
    }

    renderRow(asset, i) {
        let scope = this
        let state = this.state
        let length = this.state.assetsData.length

        return (
            <View
                key={i}
                style={{
                    flex: 1,
                    alignSelf: 'stretch',
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    borderBottomColor: 'white'
                }}>
                <View style={{ width: width / 10, padding: 7 }}>
                    <Text style={{ textAlign: 'center' }}>{length - i}</Text>
                </View>
                <View style={[styles.tableRow]}>
                    <Text style={{ width: width / 6, textAlign: 'center' }}>{asset.asset}</Text>
                </View>
                <View style={styles.tableRow}>
                    <Text style={styles.text}>{asset['power']}</Text>
                </View>
                <View style={styles.tableRow}>
                    <Text style={styles.text}>{asset['windSpeed']}</Text>
                </View>
                {/*<View style={{ flex: 1, alignSelf: 'stretch',padding: 5,borderRightWidth: 1 ,*/}
                {/*    borderRightColor: 'gray' }}>*/}
                {/*    /!*<Text>imran</Text>*!/*/}
                {/*    <Text style={styles.text}>{state.assets[key]['RPM']}</Text>*/}
                {/*</View>*/}
                {/*<View style={{ flex: 1, alignSelf: 'stretch',padding: 5,borderRightWidth: 1 ,*/}
                {/*    borderRightColor: 'gray' }}>*/}
                {/*    /!*<Text>imran</Text>*!/*/}
                {/*    <Text style={styles.text}>{state.assets[key]['DIR']}</Text>*/}
                {/*</View>*/}
                <View style={styles.tableRow}>
                    <Text style={styles.text}>{asset['UNITS']}</Text>
                </View>
                <View style={styles.tableRow}>
                    <Text style={styles.text}>{asset['TEMP']}</Text>
                </View>
            </View>
        )
    }

    assetsData() {
        {/*<TouchableOpacity*/
        }
        {/*    style={styles.card}*/
        }
        {/*    key={index}*/
        }

        {/*>*/
        }

        {/*    <View*/
        }
        {/*        style={styles.row}*/
        }
        {/*    >*/
        }
        {/*        <Text style={styles.title}>Asset Name: </Text>*/
        }
        {/*        <Text style={styles.text}>{key}</Text>*/
        }

        {/*    </View>*/
        }

        {/*    <View*/
        }
        {/*        style={styles.row}*/
        }
        {/*    >*/
        }
        {/*        <Text style={styles.title}>Power: </Text>*/
        }
        {/*        <Text style={styles.text}>{state.assets[key]['powerTag']} KW</Text>*/
        }

        {/*    </View>*/
        }

        {/*    <View*/
        }
        {/*        style={styles.row}*/
        }
        {/*    >*/
        }
        {/*        <Text style={styles.title}>Wind Speed: </Text>*/
        }
        {/*        <Text style={styles.text}>{state.assets[key]['windSpeedTag']} m/s</Text>*/
        }

        {/*    </View>*/
        }

        {/*    <View*/
        }
        {/*        style={styles.row}*/
        }
        {/*    >*/
        }
        {/*        <Text style={styles.title}>RPM: </Text>*/
        }
        {/*        <Text style={styles.text}>{state.assets[key]['RPM']}</Text>*/
        }

        {/*    </View>*/
        }

        {/*    <View*/
        }
        {/*        style={styles.row}*/
        }
        {/*    >*/
        }
        {/*        <Text style={styles.title}>DIR: </Text>*/
        }
        {/*        <Text style={styles.text}>{state.assets[key]['DIR']}</Text>*/
        }

        {/*    </View>*/
        }

        {/*    <View*/
        }
        {/*        style={styles.row}*/
        }
        {/*    >*/
        }
        {/*        <Text style={styles.title}>UNITS: </Text>*/
        }
        {/*        <Text style={styles.text}>{state.assets[key]['UNITS']}</Text>*/
        }

        {/*    </View>*/
        }
        {/*    {*/
        }
        {/*        Object.keys(key).includes('PITCH') == true &&*/
        }
        {/*        <View*/
        }
        {/*            style={styles.row}*/
        }
        {/*        >*/
        }
        {/*            <Text style={styles.title}>PITCH: </Text>*/
        }
        {/*            <Text style={styles.text}>{state.assets[key]['PITCH']}</Text>*/
        }

        {/*        </View>*/
        }
        {/*    }*/
        }

        {/*    <View*/
        }
        {/*        style={styles.row}*/
        }
        {/*    >*/
        }
        {/*        <Text style={styles.title}>TEMP: </Text>*/
        }
        {/*        <Text style={styles.text}>{state.assets[key]['TEMP']}°C</Text>*/
        }

        {/*    </View>*/
        }
        {/*</TouchableOpacity>*/
        }
    }

    render() {
        let state = this.state
        if (state.isLoading) {
            return (
                <View style={styles.horizontal}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        } else {
            // console.log(state.assetsData)
        }
        return (
            <View>
                <OfflineNotice />
                <TouchableOpacity
                    style={[
                        plantStyles.submitButton,
                    ]}
                    onPress={
                        () => this.getPlantLiveData(this.state.liveDataStatus)
                    }
                >
                    <Text style={{ color: 'white' }}>{state.text} LIVE DATA</Text>
                </TouchableOpacity>
                <ScrollView keyboardShouldPersistTaps={'handled'}>
                    <View style={styles.container}>
                        <Card
                            data={this.state.plantsLiveData[this.props.plant]}
                            plant={this.props.plant}
                            name={this.state.selectedName}
                            users={this.props.users}
                            yesGenData={this.props.yesGen}
                        />
                    </View>
                    <View style={[styles.container,
                    {
                        paddingBottom: 70,
                        borderColor: 'gray',
                        borderWidth: 1
                    }]
                    }>
                        {this.renderTableHead()}
                        {
                            state.assetsData.map((asset, index) => {
                                return this.renderRow(asset, index)
                            })
                        }
                    </View>
                </ScrollView>
            </View>
        );
    }
}
