import { StyleSheet, Dimensions } from 'react-native'
const {width, height} = Dimensions.get('window')

export default StyleSheet.create({

   horizontal: {
         position:  'absolute',
         top: '43%',
         left: '45%'
   },

  container: {
      marginTop: 60,
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',

  },

  asset: {
    padding: 10,
    backgroundColor: '#4cd137',
    color: 'white',
    width: 200,
    marginBottom: 10,
  },

  // text: {
  //   color: 'white',
  //   textAlign: 'center'
  // },

  input: {
        width: width / 1.5,
        marginBottom: 15,
        padding: 10,
        borderColor : '#aeb7bf',
        borderWidth: 1,
        borderRadius: 3
  },
    card: {
        padding: 10,
        marginBottom: 30,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 10, height: 10 },
        shadowOpacity: .7,
        shadowRadius: 2,
        elevation: 5,
        width: '80%'
    },
    valueIcon: {
        width: 30,
        height: 30
    },
    flexBox: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
        padding: 10,
        paddingBottom: 0
    },
    plantDetails: {
        borderColor: 'lightgray',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10,
        marginBottom: 10
    },
    detailsColumn: {
        alignItems: 'center',
        // borderRightWidth: 1,
        // borderColor: 'lightgray'
    },
    colorContainer: {
        backgroundColor: 'green',
        borderRadius: 10,
        width: width / 8,
        height: height / 14,
        paddingTop: height / 50,
        alignItems: 'center',
        opacity: 0.7,
        // borderWidth: 1,
    },
    values:  {
        borderTopWidth: 0,
        borderBottomWidth: 0,
        paddingRight: 30,
        paddingLeft: 30,
        margin: 0
    },
    row: {
        width: width * .78,
        padding : 7,
        flexDirection: 'row',
    },
    text: {
        fontSize: 15,
        flex: 1,
        flexWrap: 'wrap',
        textAlign: 'center'
    },

    title :{
        width: 110,
        fontSize: 15,
        fontWeight: 'bold'
    },
    submitButton: {
        backgroundColor: '#007bff',
        padding: 10,
        textAlign: 'center',
        alignItems: 'center',
    },

    tableHead: {
        flex: 1,
        // alignSelf: 'stretch',
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 15,
        // alignItems: 'center',
        // borderRightWidth: 1 ,
        // borderRightColor: 'gray',
        color: 'white'
    },
    tableRow: {
        flex: 1,
        alignSelf: 'stretch',
        padding: 7,
        marginLeft: -15,
        alignItems: 'center',
        // borderBottomWidth: 1 ,
        // borderBottomColor: 'white'
    },
    sortIcon: {
        width: 20,
        height: 20
    }

});