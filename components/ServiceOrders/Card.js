import React, { Component } from 'react'
import {
    View,
    Text,
    TouchableOpacity,

} from 'react-native'
import { Actions } from 'react-native-router-flux';
import obj from '../config.js'
import styles from './ServiceOrderCss'
import { acceptStatus as acceptStatusofSO } from '../Network/api/serviceOrdersApi'

export default class Card extends Component {
    constructor(props) {
        super(props)
        // downloadObj = new Download();
    }
    state = {
        isOpen: false,
        status: "Accepted"
    }
    viewPrognosis = (hcode, id, plant_name, asset, index, order_id, hCode, head, serviceOrderData) => {
        //    if(obj.isConnected) {
        Actions.viewServiceOrder({
            order_hCode: hcode,
            plant: plant_name,
            user_hCode: hCode,
            service: head,
            asset_id: id,
            index: index,
            asset: asset,
            data: serviceOrderData,
            order_id: order_id,
            isAccepted: !this.state.isOpen,
            pm_type_id_fk: this.props.data.pm_type_id_fk,
        })
        //        }
    }

    acceptStatus = async (hcode, user_hCode) => {
        await acceptStatusofSO(hcode, user_hCode).then((responseText) => {
            // console.log()
            // let responseObj = JSON.parse(responseText)
            if (responseText.message == 'Saved Successfully') {
                alert(responseText.message)
                // this.props.componentDidMount()
                this.setState({
                    isOpen: false,
                    status: "Accepted"
                })
            }
        })
            .catch((error) => {
                //    console.error(error);
            });
    }
    componentDidMount() {
        if (this.props.data.status == 'Open') {
            this.setState({
                isOpen: true,
                status: "Open"
            })
        }
    }

    render() {
        const titles = ['Service Id', 'Title', 'Plant Name', 'Asset Name', 'Status', 'Severity', 'Created On']
        const obj_keys = ['ser_order_num', 'title', 'plant_name', 'assets', 'status', 'severity', 'created_on']
        return (

            <TouchableOpacity
                style={styles.card}
                onPress={
                    () => this.viewPrognosis(
                        this.props.data.hcode,
                        this.props.data.asset_id,
                        this.props.data.plant_name,
                        this.props.data.assets,
                        this.props.index,
                        this.props.data.id,
                        this.props.user_hCode,
                        this.props.service,
                        this.props.serviceOrderData,
                        this.props.data.pm_type_id_fk,
                    )
                }
            >
                {
                    obj_keys.map((key, row_index) => (
                        <View
                            key={row_index}
                            style={styles.row}
                        >
                            <Text style={styles.title}>{titles[row_index]}</Text>
                            {
                                key == "status" && (this.props.data[key] == 'Open' ||
                                    this.props.data[key] == "Accepted") &&
                                < Text style={styles.text}>{this.state.status}</Text>

                            }
                            {
                                key == "status" && this.props.data[key] != "Open" && 
                                this.props.data[key] != "Accepted" &&
                                <Text style={styles.text}>{this.props.data[key]}</Text>

                            }

                {
                    key != "status" &&
                    <Text style={styles.text}>{this.props.data[key]}</Text>
                }

                {
                    key == 'status' && this.state.isOpen &&
                    <TouchableOpacity
                        style={styles.acceptButton}
                        onPress={
                            () => this.acceptStatus(this.props.data.hcode, this.props.user_hCode)
                        }
                    >
                        <Text style={styles.buttonText}>Accept</Text>
                    </TouchableOpacity>
                }
                        </View>
        ))
    }
            </TouchableOpacity>
        )
    }


}





// export default function Card(props) {
    // const titles = ['Title', 'Asset Name', 'Status', 'Severity', 'Created_on']
    // const obj_keys = ['title', 'asset_name', 'status', 'severity', 'created_on']
    // return (
    //     <TouchableOpacity
    //         style={styles.card}
    //         onPress={
    //             () => this.viewPrognosis(
    //                 props.data.hcode,
    //                 props.data.asset_id,
    //                 props.data.plant_name,
    //                 props.data.asset_name,
    //                 props.index,
    //                 props.data.id,
    //                 props.user_hCode,
    //                 props.service,
    //                 props.serviceOrderData
    //             )
    //         }
    //     >
    //         {
    //             obj_keys.map((key, row_index) => (
    //                 <View
    //                     key={row_index}
    //                     style={styles.row}
    //                 >
    //                     <Text style={styles.title}>{titles[row_index]}</Text>
    //                     <Text style={styles.text}>{props.data[key]}</Text>
    //                     {
    //                         key == 'status' && props.data.status == 'Open' &&
    //                         <TouchableOpacity
    //                             style={styles.acceptButton}
    //                             onPress={
    //                                 () => this.acceptStatus(props)
    //                             }
    //                         >
    //                             <Text style={styles.buttonText}>Accept</Text>
    //                         </TouchableOpacity>
    //                     }
    //                 </View>
    //             ))
    //         }
    //     </TouchableOpacity>
    // )
// }