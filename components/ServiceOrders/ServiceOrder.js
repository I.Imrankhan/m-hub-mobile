import React, { Component } from 'react'
import {
    View,
    Button,
    TouchableHighlight,
    ScrollView,
    Image,
    Text,
    TouchableOpacity,
    StyleSheet,
    ActivityIndicator,
    RefreshControl, Dimensions
} from 'react-native'
import styles from './ServiceOrderCss.js'
import obj from '../config.js'
import { Actions } from 'react-native-router-flux'
import NoData from '../NoDatafound/NoData.js'
import OfflineNotice from '../OfflineNotice/OfflineNotice.js'
import AsyncStorage from '@react-native-community/async-storage';
import Download from '../Network/save/Download.js'
import { fetchData, acceptStatus, downloadServiceOrders } from '../Network/api/serviceOrdersApi'
import { removeItemValue } from '../Network/api/viewServiceApi'
import DatePicker from 'react-native-datepicker'
import moment from 'moment'
import Card from './Card'
let { height, width } = Dimensions.get('window');

class ServiceOrder extends Component {
    constructor(props) {
        super(props)
        let downloadObj = new Download();
    }
    state = {
        serviceOrderData: [],
        status: true,
        dataNotFound: false,
        isLoading: true,
        refreshing: false,
        active: 1,
        isDownloading: false,
        showDatePicker: false,
        fromDate: '',
        toDate: ''
    }

    img = {
        Wind: require('../../assets/wind_unselect.png'),
        Solar: require('../../assets/solar_unselect.png')
    }

    fetchStatusData = () => {
        fetch(obj.BASE_URL + "api/controlCenter/getSerOrderStatus")
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    statusData: responseJson.data
                })
            })
            .catch((error) => {
                console.error(error);
            });
    }

    _onRefresh = () => {
        this.setState({ refreshing: true });
        if (!obj.isConnected)
            this.setState({ refreshing: false });

        this.componentDidMount();
    }


    getServiceOrders = (type, fromDate, toDate) => {
        let data = []
        this.setState({
            showDatePicker: false,
            isLoading: true
        })

        fetchData(this.props.url, this.props.hCode, type, fromDate, toDate).then(responseJson => {
            if (!responseJson.data.length) {
                this.setState({
                    dataNotFound: true,
                    isLoading: false,
                    refreshing: false,
                    serviceOrderData: []
                })
            } else {

                responseJson.data.map((item, i) => (
                    item.assets.length &&
                    data.push(
                        {
                            title: item.title,
                            assets: item.assets,
                            plant_name: item.plant_name,
                            asset_id: item.assets[0].id,
                            status: item.status[0].sname,
                            severity: item.status[0].severity,
                            created_on: item.created_on,
                            ser_order_num: item.ser_order_num,
                            id: item.id,
                            hcode: item.hcode,
                            pm_type_id_fk: item.pm_type_id_fk
                        }
                    )
                ))

                this.setState({
                    serviceOrderData: data,
                    isLoading: false,
                    refreshing: false
                })
                //                    downloadObj.downloadServiceOrders(this.state.serviceOrderData, this.state.statusData, this.props.hCode, this.props.head, 1)
            }
        })
    }


    async componentDidMount() {
        let data = []

        this.setState({
            fromDate: moment(new Date()).format('DD-MM-YYYY'),
            toDate: moment(new Date()).format('DD-MM-YYYY')
        })
        this.props.navigation.setParams({
            title: this.props.head + " Service Orders"
        })


        if (obj.isConnected) {
            this.getServiceOrders(1)
            this.fetchStatusData()
        }

        else {

            if (this.props.head == 'Manual') {
                await AsyncStorage.getItem('manual').then((value) => {

                    if (value == null || !JSON.parse(value).manualServices.length) {
                        this.setState({
                            isLoading: false
                        })
                    }
                    else {
                        this.setState({
                            serviceOrderData: JSON.parse(value).manualServices,
                            isLoading: false
                        })
                    }
                })
            }

            else {
                await AsyncStorage.getItem('prognosis').then((value) => {
                    if (value == null || !JSON.parse(value).prognosisServices.length) {
                        this.setState({
                            isLoading: false
                        })
                    }

                    else {
                        this.setState({
                            serviceOrderData: JSON.parse(value).prognosisServices,
                            isLoading: false
                        })
                    }
                })
            }
        }
        this.active(1)
        removeItemValue("serOrderFileIds")
        
    }


    acceptStatus = (hcode) => {

        acceptStatus(hcode, this.props.hCode).then((responseText) => {
            let responseObj = JSON.parse(responseText)
            if (responseObj.message == 'Saved Successfully') {
                alert(responseObj.message)
                this.componentDidMount()
            }
        })
            .catch((error) => {
                //            console.error(error);
            });
    }

    viewPrognosis = (hcode, id, plant_name, asset, index, order_id) => {
        //        if(obj.isConnected) {
        Actions.viewServiceOrder({
            order_hCode: hcode,
            plant: plant_name,
            user_hCode: this.props.hCode,
            service: this.props.head,
            asset_id: id,
            index: index,
            asset: asset,
            data: this.state.serviceOrderData,
            order_id: order_id
        })
        //        }
    }

    download() {
        let state = this.state
        let scope = this

        if (obj.isConnected) {
            scope.setState({
                isDownloading: true
            })
            let manualServiceDetails = []
            downloadServiceOrders(
                state.serviceOrderData, state.statusData, this.props.hCode, this.props.head,
                state.active, state.fromDate, state.toDate
            ).then(response => {
                if (response) {
                    scope.setState({
                        isDownloading: false
                    })
                }
                alert('Data saved successfully')
            })
        }
        else {
            scope.setState({
                isDownloading: false
            })
        }
    }

    
    active = (type) => {
        this.setState({
            active: type,
            dataNotFound: false,
            // isLoading: true,
            serviceOrderData: []
        })

        if (type == 4) {
            this.setState({
                showDatePicker: true,
            })
        }

        else {
            this.setState({
                showDatePicker: false
            })
        }

        if (type != 4)
            this.getServiceOrders(type)
    }

    customDate = (type, fromDate, toDate) => {
        this.setState({
            showDatePicker: false
        })
        let startYear = parseInt(fromDate.split('-')[2]);
        let startMonth = parseInt(fromDate.split('-')[1]);
        let startDay = parseInt(fromDate.split('-')[0]);

        let endYear = parseInt(toDate.split('-')[2]);
        let endMonth = parseInt(toDate.split('-')[1]);
        let endDay = parseInt(toDate.split('-')[0]);

        let startDate = { "date": { "year": startYear, "month": startMonth, "day": startDay } }
        let endDate = { "date": { "year": endYear, "month": endMonth, "day": endDay } }

        this.getServiceOrders(type, startDate, endDate)
    }

    closeDatePicker = () => {
        let scope = this
        scope.setState({
            showDatePicker: false,
            active: 1
        })
        scope.getServiceOrders(1)
    }

    changeDate = (date, type) => {
        if (type == 'from') {
            this.setState({
                fromDate: date
            })
        }
        else {
            this.setState({
                toDate: date
            })
        }
    }


    render() {
        let today = new Date()
        //        if(this.state.serviceOrderData.length)
        //            alert(this.state.serviceOrderData[0].id)
        const titles = ['Service Number', 'Title', 'Asset Name', 'Status', 'Severity', 'Created_on']
        const obj_keys = ['ser_order_num','title', 'asset_name', 'status', 'severity', 'created_on']
        // if(this.state.dataNotFound) {
        //     return(
        //         <NoData />
        //     )
        // }

        // if(this.state.isLoading) {
        //     return (
        //         <View style = {styles.horizontal}>
        //             <ActivityIndicator size="large" color="#0000ff" />
        //         </View>
        //     )
        // }

        return (
            <View>
                {
                    this.state.isDownloading &&
                    <View style={styles.blur}>
                        <View style={styles.loaderContainer}>
                            <View style={styles.downloading}>
                                <ActivityIndicator style={{ marginRight: 30, marginLeft: -30 }} size="large" color="#0000ff" />
                                <Text>downloading...</Text>
                            </View>
                        </View>
                    </View>

                }

                {
                    this.state.showDatePicker &&
                    <View style={[styles.blur]}>
                        <View style={[styles.datePicker]}>
                            <View style={[styles.downloading, { flex: 1, flexDirection: 'column' }]}>

                                <TouchableOpacity
                                    onPress={() => { this.closeDatePicker() }}
                                    style={[styles.row, { marginLeft: 50, marginBottom: 20, marginTop: 0, flex: 1, justifyContent: 'flex-end' }]}>
                                    <Image style={{ width: 30, height: 30 }} source={require('../../assets/close.png')}></Image>
                                </TouchableOpacity>

                                <View style={[styles.row, {
                                    width: '100%',
                                    marginBottom: width / 10
                                }]}>
                                    <Text style={styles.dateTitle}>From</Text>
                                    <DatePicker
                                        style={{ width: '73%' }}
                                        date={this.state.fromDate}
                                        mode="date"
                                        format="DD-MM-YYYY"
                                        maxDate={today}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: '85%',
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                padding: 23,
                                                borderColor: '#AFAAAA',
                                                borderRadius: 5
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => { this.changeDate(date, 'from') }}
                                    />
                                </View>

                                <View style={[styles.row, {
                                    width: '100%',
                                    marginBottom: width / 20
                                }]}>
                                    <Text style={styles.dateTitle}>To</Text>
                                    <DatePicker
                                        style={{ width: '73%' }}
                                        date={this.state.toDate}
                                        mode="date"
                                        format="DD-MM-YYYY"
                                        minDate={this.state.fromDate}
                                        maxDate={today}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: '85%',
                                                top: 4,
                                                marginLeft: 0
                                            },

                                            dateInput: {
                                                padding: 23,
                                                borderColor: '#AFAAAA',
                                                borderRadius: 5
                                            }
                                        }}
                                        onDateChange={(date) => { this.changeDate(date, 'to') }}
                                    />
                                </View>

                                <View style={styles.row}>
                                    <TouchableOpacity
                                        style={[styles.submitButton,
                                        { marginLeft: -5, marginBottom: 20, width: '105%', borderRadius: 5 }
                                        ]}
                                        onPress={() => this.customDate(4, this.state.fromDate, this.state.toDate)}
                                    >
                                        <Text style={{ color: 'white', textAlign: 'center' }}>SHOW</Text>
                                    </TouchableOpacity>

                                </View>
                            </View>
                        </View>
                    </View>
                }
                <OfflineNotice />

                {
                    // obj.isConnected &&
                    <View style={{ paddingTop: 0 }}>
                        <View style={styles.menu}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.active(1)
                                }}
                                style={[styles.option,
                                this.state.active === 1 ? styles.active : null
                                ]}>
                                <Text style={{ color: this.state.active === 1 ? 'white' : null }}>48 hrs</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.active(2)
                                }}
                                style={[styles.option,
                                this.state.active === 2 ? styles.active : null
                                ]}>
                                <Text style={{ color: this.state.active === 2 ? 'white' : null }}>1 week</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.active(3)
                                }}
                                style={[styles.option,
                                this.state.active === 3 ? styles.active : null
                                ]}>
                                <Text style={{ color: this.state.active === 3 ? 'white' : null }}>MTD</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => {
                                    this.active(4)
                                }}
                                style={[styles.option,
                                this.state.active === 4 ? styles.active : null
                                ]}>
                                <Text style={{ color: this.state.active === 4 ? 'white' : null }}>Custom Date</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                }

                {
                    obj.isConnected &&
                    <TouchableOpacity
                        style={[
                            styles.submitButton,
                        ]}
                        onPress={
                            () => this.download()
                        }
                    >
                        <Text style={{ color: 'white' }}>Download Data</Text>
                    </TouchableOpacity>
                }

                {
                    this.state.serviceOrderData.length > 0 &&
                    <View style={{ alignItems: 'center', marginBottom: 10 }}>
                        <Text style={{ fontSize: 20 }}>
                            Service orders loaded: {this.state.serviceOrderData.length}
                        </Text>
                    </View>
                }

                <ScrollView
                    // refreshControl={
                    //     <RefreshControl
                    //         refreshing={this.state.refreshing}
                    //         onRefresh={this._onRefresh}
                    //         colors={['#007bff', 'red', 'green', 'yellow']}
                    //     />
                    // }
                >
                    <View style={[styles.container]}>

                        {/*<Download />*/}

                        {
                            this.state.serviceOrderData.map((item, index) => (
                                <Card
                                    key = {index}
                                    data = {item}
                                    user_hCode = {this.props.hCode}
                                    service = {this.props.head}
                                    serviceData = {this.state.serviceOrderData}
                                    index = {index}
                                    componentDidMount={this.componentDidMount}
                                />
                            ))

                        }

                        {
                            this.state.dataNotFound &&
                            <View style={{ marginTop: height / 4, marginBottom: height / 5 }}>
                                <NoData />
                            </View>
                        }

                        {
                            this.state.isLoading &&
                            <View style={styles.horizontal}>
                                <ActivityIndicator size="large" color="#0000ff" />
                            </View>

                        }
                    </View>
                </ScrollView>
            </View>
        )
    }
}
export default ServiceOrder
