import { StyleSheet, Dimensions } from 'react-native'
let {height, width} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        marginTop: 30,
        marginBottom: height / 5,
    },
    card: {
        padding: 10,
        marginBottom: 30,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 10, height: 10 },
        shadowOpacity: .7,
        shadowRadius: 2,
        elevation: 5
    },
    row: {
        width: width * .78,
        padding : 7,
        flexDirection: 'row',
    },


    text: {
        fontSize: 15,
        flex: 1,
        flexWrap: 'wrap'
    },

    title :{
        width: 110,
        fontSize: 15,
        fontWeight: 'bold'
    },

    plantName: {
        fontWeight: 'bold'
    },

    horizontal: {
        // position:  'absolute',
        // top: '43%',
        // left: '45%'
        marginTop: height / 6
    },

    acceptButton: {
        width: '50%',
        flex:1 ,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        marginTop: -5,
        padding: 5,
        marginLeft: 10,
        backgroundColor: '#4cd137'
    },

    buttonText: {
        color: 'white'
    },
    imgContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    submitButton: {
        marginBottom: 10,
        backgroundColor: '#176494',
        padding: 10,
        textAlign: 'center',
        alignItems: 'center',
    },

    loaderContainer: {
        position: 'absolute',
        left: width / 10,
        top: height / 3,
        borderRadius: 10,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '80%',
        height: 100,
        backgroundColor: 'white',
    },

    datePicker: {
        position: 'absolute',
        width: '90%',
        left: width / 20,
        top: height / 5,
        borderRadius: 10,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        // paddingTop: width / 15
    },

    blur: {
        position: 'absolute',
        width: width,
        height: height,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        zIndex: 20
    },

    downloading: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    menu: {
        // marginTop: 20,
        flexDirection: 'row',
        // justifyContent: 'space-around',
        backgroundColor: 'white',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: 'white'
    },

    option: {
        padding: width / 17,
        paddingTop: width / 40,
        paddingBottom: width / 25,
        // backgroundColor: 'white'
        // borderRightWidth: 1,
        // alignItems: 'center'
    },
    active: {
        color: 'white',
        backgroundColor: '#4cd137'
    },
    dateTitle: {
        fontSize: 20,
        marginTop: 5,
        marginLeft: 20,
        width:  width / 7,
        fontWeight: 'bold'
    },

})