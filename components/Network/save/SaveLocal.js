import React, { Component } from 'react'
import { AsyncStorage } from 'react-native'

export default class SaveLocal extends Component {

    saveCheckListLocally = (serviceType, plant, index, checkListData) => {
        if(serviceType == 'Prognosis') {
            AsyncStorage.getItem( 'prognosis' )
                .then( JSONData => {
                JSONData = JSON.parse(JSONData);

                JSONData.prognosisServiceDetails[0][plant][index].checklist = checkListData
                JSONData.prognosisServiceDetails[0][plant][index].checklist[0]['isChanged'] = 1
                AsyncStorage.setItem('prognosis', JSON.stringify(JSONData))
                alert('Updated successfully')
                }).done();

        }

        else {
            AsyncStorage.getItem( 'manual' )
            .then( JSONData => {
            JSONData = JSON.parse(JSONData);
            JSONData.manualServiceDetails[0][plant][index].checklist = checkListData
            JSONData.manualServiceDetails[0][plant][index].checklist[0]['isChanged'] = 1
            AsyncStorage.setItem('manual', JSON.stringify(JSONData))
            alert('Updated successfully')

            }).done();
        }

    }

    saveBundledLocally = (plant, index, bundledActivitiesData) => {
        AsyncStorage.getItem( 'manual' )
            .then( JSONData => {
                JSONData = JSON.parse(JSONData);
                JSONData.manualServiceDetails[0][plant][index].bundled_activities = bundledActivitiesData
                JSONData.manualServiceDetails[0][plant][index].bundled_activities[0]['isChanged'] = 1
                AsyncStorage.setItem('manual', JSON.stringify(JSONData))
                alert('Updated successfully')

        }).done();
    }

    saveDownloadFilesLocally = (serviceType, plant, index, comment) => {
        if(serviceType == 'Manual') {
            AsyncStorage.getItem( 'manual' )
                .then( JSONData => {
                    JSONData = JSON.parse(JSONData);
                    let files = JSONData.manualServiceDetails[0][plant][index].Download_Files
                    files.push({comments: comment, sname: 'Accepted', isAdded: 1, created_on: new Date(), files:[] })
                    JSONData.manualServiceDetails[0][plant][index].Download_Files = files
                    AsyncStorage.setItem('manual', JSON.stringify(JSONData))
                    alert('Saved successfully')

            }).done();
        }

        else {
//            let date = new Date()
            AsyncStorage.getItem( 'prognosis' )
                .then( JSONData => {
                JSONData = JSON.parse(JSONData);
                let files = JSONData.prognosisServiceDetails[0][plant][index].Download_Files
                files.push({comments: comment, sname: 'Accepted', isAdded: 1, created_on: new Date(), files:[] })
                JSONData.prognosisServiceDetails[0][plant][index].Download_Files = files
                AsyncStorage.setItem('prognosis', JSON.stringify(JSONData))
                alert('Saved successfully')
            }).done();

        }
    }


   setIsChanged = (serviceType, plant, checkListData, index, type, isChanged) => {
        if(serviceType == 'Prognosis') {
            AsyncStorage.getItem( 'prognosis' )
                .then( JSONData => {
                    JSONData = JSON.parse(JSONData);
                    if(isChanged) {
                        if(type == 'checklist')
                            JSONData.prognosisServiceDetails[0][plant][index].checklist = checkListData
                    }
                    if(isChanged == undefined)
                        JSONData.prognosisServiceDetails[0][plant][index].checklist[0]['isChanged'] = 0
                    AsyncStorage.setItem('prognosis', JSON.stringify(JSONData))
            }).done();
        }

        else {
            AsyncStorage.getItem( 'manual' )
                .then( JSONData => {
                    JSONData = JSON.parse(JSONData);
                    if(isChanged) {
                        if(type == 'checklist')
                            JSONData.manualServiceDetails[0][plant][index].checklist = checkListData
                        else
                            JSONData.manualServiceDetails[0][plant][index].bundled_activities = checkListData
                    }
                    if(isChanged == undefined) {
                        if(type == 'checklist')
                            JSONData.manualServiceDetails[0][plant][index].checklist[0]['isChanged'] = 0
                        else
                            JSONData.manualServiceDetails[0][plant][index].bundled_activities[0]['isChanged'] = 0

                    }
                    AsyncStorage.setItem('manual', JSON.stringify(JSONData))
                }).done();
        }
   }

    setIsAdded = (serviceType, asyncData, plant, serArr) => {
        asyncData[0][plant].map((item, index) => {
            if(item.ser_order_num == serArr[0].ser_order_num) {
                i = index
            }
        })

        if(serviceType == 'Manual') {
            AsyncStorage.getItem( 'manual' )
                   .then( JSONData => {
                   JSONData = JSON.parse(JSONData);
                   let files = JSONData.manualServiceDetails[0][plant][i].Download_Files

                   files.map((item) => {
                       item.isAdded = 0
                   })
                   JSONData.manualServiceDetails[0][plant][i].Download_Files = files
                   AsyncStorage.setItem('manual', JSON.stringify(JSONData))
              })
        }

        else {
            AsyncStorage.getItem( 'prognosis' )
            .then( JSONData => {
                JSONData = JSON.parse(JSONData);
                let files = JSONData.prognosisServiceDetails[0][plant][i].Download_Files
                files.map((item) => {
                    item.isAdded = 0
                    // console.log(item)
                })
                JSONData.prognosisServiceDetails[0][plant][i].Download_Files = files
                AsyncStorage.setItem('prognosis', JSON.stringify(JSONData))
            })
        }
    }

    render(){
        return null;
    }
}
