import React, { Component } from 'react'
import { Text } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import obj from '../../config.js'
//import axios from 'axios'

export default class Download extends Component {
    constructor(props) {
        super(props)
        state = {
            isLoading: false
        }
    }

    count = 0
    manualServiceData = {}

    componentWillMount() {
        this.setState({
            isLoading: false
        })
    }

    downloadServiceOrders = async (data, statusData, user_hCode, serviceType, type, startDate, endDate) => {
        let scope = this
        if (obj.isConnected) {
            const response = await fetch(obj.BASE_URL + "api/controlCenter/" + url + hCode, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "filterType": type,
                    "startDate": startDate,
                    "endDate": endDate
                })
            });
            const json = await response.json();
            if (serviceType == 'Manual') {
                AsyncStorage.setItem('manual', JSON.stringify({
                    manualServiceDetails: json.data,
                    manualServices: data,
                    statusData: statusData
                }))
            }
            else {
                //                    AsyncStorage.removeItem('prognosis')
                AsyncStorage.setItem('prognosis', JSON.stringify({
                    prognosisServiceDetails: json.data,
                    prognosisServices: data,
                    statusData: statusData
                }))
            }
            // alert('Data saved successfully')
            return 1;
        }
    }

    saveCheckList = (order_hCode) => {
        const data = this.state.checkListData
        const checked = this.state.checked
        url = ''
        data.map((item, index) => {
            if (this.props.service == 'Prognosis') {
                url = 'mskSaveChecklist'
                if (checked[index])
                    item['isComplete'] = "1"
                else
                    item['isCompleted'] = "0"
            }
            else {
                url = 'SaveBundledActivity'
                if (checked[index])
                    item['astatus'] = true
                else
                    item['astatus'] = false
            }
        })

        if (obj.isConnected) {
            fetch(obj.BASE_URL + "api/controlCenter/" + url, {
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/json',
                }),
                body: JSON.stringify({
                    "user_hcode": obj.user_hCode,
                    "order_hcode": order_hCode,
                    "checkListArray": data
                })
            })
                .then((response) => response.text())
                .then((responseText) => {
                    responseObj = JSON.parse(responseText)
                    alert(responseObj.message)
                    if (this.props.service == 'Prognosis')
                        this.fetchCheckList()
                    else
                        this.fetchBundledActivites()
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    }

    saveServiceStatus = (order_hCode) => {
        if (obj.isConnected) {
            fetch(obj.BASE_URL + "api/controlCenter/updateStatus", {
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/json',
                }),
                body: JSON.stringify({
                    "severity_id_fk": "1",
                    "user_hcode": obj.user_hCode,
                    "order_hcode": order_hCode,
                    "comments": this.state.comments,
                    "status_id_fk": "2",
                })
            })
                .then((response) => response.text())
                .then((responseText) => {
                    responseObj = JSON.parse(responseText)
                    alert(responseObj.message)
                    this.fetchOrderLogsData()
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    }

    handleComments = (text) => {
        this.setState({
            comments: text
        })
    }

    updatePunchPoints = (index) => {
        const punchPoint = this.state.punchPoints[index]
        if (obj.isConnected) {
            fetch(obj.BASE_URL + "api/controlCenter/updatePunchPoints/" + punchPoint.hcode, {
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/json',
                }),

                body: JSON.stringify({
                    "user_hcode": obj.user_hCode,
                    "points": punchPoint.points
                })
            })
                .then((response) => response.text())
                .then((responseText) => {
                    responseObj = JSON.parse(responseText)
                    alert(responseObj.message)
                    this.fetchPunchPoints()
                })
                .catch((error) => {
                    alert(error);
                });
        }
    }



    render() {
        if (this.state.isLoading) {
            return <Text>Downloading...</Text>
        }
        return null
    }


}
