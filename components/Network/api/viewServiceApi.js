import obj from "../../config";
import { AsyncStorage } from 'react-native'
import { file } from "@babel/types";

export const fetchPunchPoints = async (hCode) => {
    const response = await fetch(obj.BASE_URL + "api/controlCenter/getSerOrderPunchPointResp/" + hCode)
    const json = await response.json();
    return json.data
}

export const fetchCheckList = async (hCode) => {
    // console.log(hCode)
    const response = await fetch(obj.BASE_URL + "api/controlCenter/getSerOrderChecklistResp/" + hCode)
    const json = await response.json()
    return json.data
    // const response = await fetch(obj.BASE_URL + "api/controlCenter/getPrognosisChecklist", {
    //     method: 'POST',
    // headers: new Headers({
    //         'Content-Type': 'application/json',
    //     }),
    //     body: JSON.stringify({
    //         "prog_rule_name": rule_name
    //     })
    // })
    // const json = await response.json()
    // console.log('check list')
    // console.log(json)
    // return json.data
}

export const getPrognosisChecklist = async (rule_name) => {
    const response = await fetch(obj.BASE_URL + "api/controlCenter/getPrognosisChecklist", {
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/json',
        }),
        body: JSON.stringify({
            "prog_rule_name": rule_name
        })
    })
    const json = await response.json()
    // console.log('getPrognosisChecklist')
    // console.log(json)
    return json.data
}

export const fetchBundledActivities = async (hCode, pm_type_id_fk) => {
    const response = await fetch(`${obj.BASE_URL}api/controlCenter/viewAllBundledActivities/${pm_type_id_fk}/${hCode}`)
    const json = await response.json()
    console.log(json.data)
    return json.data
}

export const fetchServiceOrderStatus = async () => {
    const response = await fetch(obj.BASE_URL + "api/controlCenter/getSerOrderStatus")
    const json = await response.json()
    return json.data
}


export const fetchOrderLogsData = async (hCode) => {
    const response = await fetch(obj.BASE_URL + "api/controlCenter/getDownloadFiles/" + hCode)
    const json = await response.json()
    return json.data
}

export const saveServiceOrderLogFile = async (file, id) => {
    let uriArr = file.uri.split('/')
    let fileName = uriArr[uriArr.length - 1]
    let data = new FormData();
    data.append("file", {
        uri: file.uri,
        type: "image/*",
        name: fileName,
    });
    const response = await fetch(`${obj.BASE_URL}api/controlCenter/SaveSerOrderAttachment/${id}`, {
        method: 'POST',
        headers: new Headers({
            "Accept": "application/x-www-form-urlencoded",
        }),
        body: data
    })
    const json = await response.json()
    return json
}


// export const updateSerOrderLogId = async (id) => {
//     let serOrderFileIds = []
//     await AsyncStorage.getItem('serOrderFileIds').then((data) => {
//         serOrderFileIds = JSON.parse(data)
//     })
//     if (serOrderFileIds.length) {
//         const response = await fetch(obj.BASE_URL + `api/controlCenter/updateSerOrderLogId/${id}`, {
//             method: 'POST',
//             headers: new Headers({
//                 'Content-Type': 'application/json',
//             }),
//             body: JSON.stringify({
//                 "fileIds": serOrderFileIds
//             })
//         })
//         const json = await response.json()

//         removeItemValue("serOrderFileIds")
//         return json
//     }
//     else {
//         return { message: "Successful" }
//     }
// }

export const removeItemValue = async (key) => {
    try {
        await AsyncStorage.removeItem(key);
        return true;
    }
    catch (exception) {
        return false;
    }
}


export const updateStatus = async (file, data) => {
    const getStatusId = await fetch(`${obj.BASE_URL}api/controlCenter/getLastStatusIdByServiceOrder/${data.order_hCode}`)
    const statusIdJson = await getStatusId.json()
    const statusId = statusIdJson.data.status_id_fk
    const response = await fetch(obj.BASE_URL + "api/controlCenter/updateStatus", {
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/json',
        }),
        body: JSON.stringify({
            "severity_id_fk": "1",
            "user_hcode": data.user_hCode,
            "order_hcode": data.order_hCode,
            "comments": "File Upload",
            "status_id_fk": statusId,
        })
    })
    const json = await response.json()
    let fileUploadStatus = await saveServiceOrderLogFile(file, json.data)
    return fileUploadStatus
}

export const saveOrderLogPhotoLocally = async (file, data) => {
    let serOrderFiles = []
    await AsyncStorage.getItem('serOrderFiles').then((data) => {
        if (data) {
            serOrderFiles = JSON.parse(data)
        }
    })
    serOrderFiles.push({
        file, data
    })
    await AsyncStorage.setItem('serOrderFiles', JSON.stringify(serOrderFiles))
    return "Photo Saved Locally"

}

export const uploadLocallySavedFiles = async (data) => {
    let fileData = JSON.parse(data)
    let runLoop = 1
    let index = 0;
    // await fileData.map(async item => {
    while (runLoop) {
        let res = await updateStatus(fileData[index].file, fileData[index].data)
        if(res) {
            index++;
        }
        if(index == fileData.length) {
            runLoop = 0;
        }
    }
    // })
    AsyncStorage.removeItem("serOrderFiles")
    return 1;
}

export const uploadPmChecklistFile = async(image, id) => {
    let uriArr = image.uri.split('/')
    let fileName = uriArr[uriArr.length - 1]
    let data = new FormData();
    data.append("file", {
        uri: image.uri,
        type: "image/*",
        name: fileName,
    });

    const response = await fetch(`${obj.BASE_URL}api/controlCenter/uploadPmBundledFiles/${id}`, {
        method: 'POST',
        headers: new Headers({
            "Accept": "application/x-www-form-urlencoded",
        }),
        body: data
    })
    const json = await response.json()
    return json
}

export const deletePmFile = async(id) => {
    const response = await fetch(obj.BASE_URL + "api/controlCenter/deletePmBundledFile/" + id)
    const json = await response.json()
    return json
}