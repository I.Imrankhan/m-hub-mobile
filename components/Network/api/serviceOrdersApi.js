import obj from "../../config";
import AsyncStorage from '@react-native-community/async-storage';

export const fetchData = async (url, hCode, type, startDate, endDate) => {
    const response = await fetch(obj.BASE_URL + "api/controlCenter/" + url + hCode, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "filterType": type,
            "startDate": startDate,
            "endDate": endDate
        })
    });
    const json = await response.json();
    let assets = ''
    for(let i = 0; i < json.data.length; i++) {
        if (typeof json.data[i].assets == "object") {
            assets = ''
            json.data[i].assets.map((ass, index) => {
                assets += ass.itemName;
                if(index < json.data[i].assets.length - 1) {
                    assets += ','
                }
            })
            json.data[i].assets = assets
        }
    }
    return json
}

export const acceptStatus = async (hcode, user_code) => {
    const response = await fetch(obj.BASE_URL + "api/controlCenter/updateSerOrderLog", {
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/json',
        }),
        body: JSON.stringify({
            "severity_id_fk": "1",
            "comments": "",
            "status_id_fk": "2",
            "order_hcode": hcode,
            "user_hcode": user_code,
            "pm_type_id_fk": ""
        })
    })
    const json = await response.json()
    // console.log(json)

    return json
}

export const downloadServiceOrders = async (data, statusData, user_hCode, serviceType, type, startDate, endDate) => {
    if (obj.isConnected) {
        // console.log(startDate, endDate)

        const response = await fetch(`${obj.BASE_URL}api/controlCenter/getServiceOrderByUserHcode/${user_hCode}`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "filterType": type,
                "startDate": startDate,
                "endDate": endDate
            })
        });
        const json = await response.json();
        if (serviceType == 'Manual') {
            AsyncStorage.setItem('manual', JSON.stringify({
                manualServiceDetails: json.data,
                manualServices: data,
                statusData: statusData
            }))
        } else {
            AsyncStorage.setItem('prognosis', JSON.stringify({
                prognosisServiceDetails: json.data,
                prognosisServices: data,
                statusData: statusData
            }))
        }
        return 1;

    }
}

export const getUsersofPlants = async (id) => {
    const response = await fetch(obj.BASE_URL+ "api/operation/getAllOperationUserByPlantId/"+id)
    const json = await response.json();
    return json
}