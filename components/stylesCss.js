import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        paddingTop: 20,
        flexGrow: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    margin: {
        marginBottom: 30
    },

    card: {
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 30,
    },

    row: {
        padding : 7,
        flexDirection: 'row',
        width: '85%',
    },

    rowCenter: {
        alignItems: 'center'
    },

    text: {
        fontSize: 15,
        flex: 1,
        flexWrap: 'wrap',
    },

    title: {
        width: 170,
        fontSize: 16,
        fontWeight: 'bold'
    },
    horizontal: {
        position:  'absolute',
        top: '43%',
        left: '45%'
    },

    notification: {
        width: '100%',
        // borderWidth: .2,
        marginBottom: 0,
        borderRadius: 0
    },

})