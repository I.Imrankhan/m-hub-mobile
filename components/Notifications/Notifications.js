import React, { Component } from 'react'
import {
    View, ScrollView, Text, AsyncStorage, RefreshControl, ActivityIndicator
} from 'react-native'
import Footer from '../Footer/Footer.js'
import OfflineNotice from '../OfflineNotice/OfflineNotice.js'
import obj from '../config.js'
import moment from 'moment';
import styles from '../stylesCss'
import Swipeout from 'react-native-swipeout';
import NoData from "../NoDatafound/NoData";

export default class Notifications extends Component {
    state = {
        showLoader: true,
        refreshing: false,
        notifications: [],
    }

    componentDidMount() {
        let data;
        // AsyncStorage.removeItem(obj.user_hCode+'notification')
        AsyncStorage.getItem(obj.user_hCode+'notification').then((value) => {
            if(JSON.parse(value)) {
                // console.log(JSON.parse(value))
                data = JSON.parse(value).sort(this.sortByDescending)
                this.setState({
                    notifications: data,
                    refreshing: false,
                    showLoader: false
                })
            }
            else {
                this.setState({
                    showLoader: false
                })
            }
        })
    }

    sortByDescending = (a, b) => {
        if ((a.created_on) < (b.created_on))
            return 1;
        if ((a.created_on) > (b.created_on))
            return -1;
        return 0;
    }


    _onRefresh = () => {
        this.setState({refreshing: true});
        // if(!obj.isConnected)
        //     this.setState({refreshing: false});

        this.componentDidMount();
    }

    deleteNotification(index) {
        // console.log('delete notification '+ index)
        let arr = this.state.notifications
        arr.splice(index,1)
        // console.log(arr)
        this.setState({
            notifications: arr
        })
        AsyncStorage.setItem(obj.user_hCode+'notification', JSON.stringify(arr))
    }

    row = (data, index) => {
        let swipeoutBtns = [
            {
                text: 'Delete',
                backgroundColor: 'red',
                onPress: () => this.deleteNotification(index)
            }
        ]
        return(
            <Swipeout style = {{borderRadius: 10, paddingLeft: 7}} backgroundColor = '#d7d9da' right={swipeoutBtns} autoClose={true} >

                <View style = {[styles.row]}>
                    <Text style = {{width: '65%', fontWeight: 'bold'}} >{data.title}</Text>
                    <Text>{moment(data.created_on).format('lll')}</Text>
                </View>

                <View style = {[styles.row, {width: '100%'}]}>
                    <Text>{data.body}</Text>
                </View>

            </Swipeout>
        )
    }

    render() {

        if(this.state.showLoader) {
            return (
                <View style = {styles.horizontal}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        }

        else if(!this.state.notifications.length) {
            return(
                    <NoData/>
            )
        }
        return (
            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                        colors={['#007bff']}
                    />
                }
            >

                <OfflineNotice />
                {
                    this.state.notifications.length > 0 &&
                    <View style={[styles.container, {marginTop: 50, paddingBottom: 50}]}>
                        {
                            this.state.notifications.map((notification, index) => (
                                <View style={[styles.card, styles.notification ]} key={index}>
                                    {this.row(notification, index)}
                                </View>
                            ))
                        }
                    </View>
                }
            </ScrollView>
        )
    }
}
