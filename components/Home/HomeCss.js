import { StyleSheet, Dimensions } from 'react-native'

let { height , width } = Dimensions.get('window')
export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
//        alignItems: 'center'
    },
   icon: {
        height: height / 13,
        width: width / 8
   },

   each: {
       backgroundColor: 'white',
       width: width / 4,
       height: height / 8,
       marginRight: width / 8,
       borderRadius: 10,
       // marginBottom: width / 10,
       shadowColor: '#000',
       shadowOffset: { width: 10, height: 10 },
       shadowOpacity: .7,
       shadowRadius: 2,
       elevation: 5
   },
   iconContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        marginLeft: width / 8,
        marginBottom: height / 10
   },

   row: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
       justifyContent: 'center',
   },

   column: {
        flex: 1,
        justifyContent : 'center',
        alignItems: 'center',
//        borderColor: '#d6d7da',
//        borderWidth: 1,
        borderRadius: 3,
   },
    logout: {
        padding: 10,
        paddingRight: 30,
        paddingLeft: 10,
        backgroundColor: '#176494',
        // alignItems: 'center'
    }
})