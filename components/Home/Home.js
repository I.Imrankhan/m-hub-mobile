import React, { Component } from 'react'
import {
    View,
    ScrollView,
    Image,
    Text,
    StyleSheet,
    TouchableOpacity,
    AsyncStorage,
    BackHandler,
    Alert,
    Dimensions,
    Linking,
    Animated,
    Easing
} from 'react-native'
import Header from '../Header/Header.js'
import Footer from '../Footer/Footer.js'
import { Col, Row, Grid } from "react-native-easy-grid"
import BASE_URL from '../config.js'
import { Actions } from 'react-native-router-flux';
import styles from './HomeCss.js'
import OfflineNotice from '../OfflineNotice/OfflineNotice.js'
import obj from '../config.js'
import firebase from "react-native-firebase";
import { removeItemValue } from '../Network/api/viewServiceApi'


let { height, width } = Dimensions.get('window')

class Home extends Component {
    state = {
        showLoader: false,
        name: ''
    }
    spinValue = new Animated.Value(0)
    spin

    getAllTool = () => {
        if (obj.isConnected)
            Actions.list({ url: 'getAllTool', list: 'Tools' })
    }

    getAllMaterial = () => {
        if (obj.isConnected)
            Actions.list({ url: 'getAllMaterial', list: 'Materials' })
    }

    getAssetsByUserId = () => {
        // AsyncStorage.removeItem('demo')
        if (obj.isConnected)
            Actions.plants({ hCode: obj.user_hCode })
    }

    getPrognosisServiceOrder = () => {
        //        if(obj.isConnected)
        Actions.serviceOrder({ url: 'getPrognosisServiceOrder/', hCode: obj.user_hCode, head: 'Prognosis' })
    }

    getManualServiceOrder = () => {
        //        if(obj.isConnected)
        Actions.serviceOrder({ url: 'getManualServiceOrder/', hCode: obj.user_hCode, head: 'Manual' })
    }

    goToNotifications = () => {
        Actions.notifications()
    }

    subscribeToPlant = (topic) => {
        // console.log(topic)
        firebase.messaging().subscribeToTopic(topic)
            .then(function (response) {
                // See the MessagingTopicManagementResponse reference documentation
                // for the contents of response.
                // console.log('Successfully subscribed to topic:', response);
            })
            .catch(function (error) {
                console.log('Error subscribing to topic:', error);
            });
    }

    fetchPlants = () => {
        fetch(obj.BASE_URL + "api/controlCenter/getAssetByUserId/" + obj.user_hCode)
            .then((response) => response.json())
            .then((responseJson) => {
                // console.log(responseJson)
                if (responseJson.data.length) {
                    responseJson.data.map((item) => {
                        this.subscribeToPlant(item.plant_name.toLowerCase())
                    })
                }
            })
    }




    componentDidMount() {
        console.log("AJS")
        if (obj.isConnected)
            this.fetchPlants()
        // AsyncStorage.removeItem('user')
        AsyncStorage.getItem('user').then((data) => {
            this.setState({
                name: JSON.parse(data).name
            })
        })
        removeItemValue("serOrderFileIds")

    }


    logout = () => {
        AsyncStorage.removeItem('user')
        Actions.login()
    }

    takePicture = () => {
        Actions.takePhoto()

    }

    render() {

        return (
            <View style={styles.container}>
                <OfflineNotice />
                <View style={styles.iconContainer}>
                    <Grid>
                        <Row style={[styles.row]}>
                            <Col style={styles.each}>
                                <TouchableOpacity
                                    style={styles.column}
                                    onPress={
                                        () => this.getPrognosisServiceOrder()
                                    }
                                >
                                    <Image style={styles.icon} source={require('../../assets/prognosis.png')} />
                                    <Text>Prognosis</Text>
                                </TouchableOpacity>
                            </Col>

                            <Col style={styles.each}>
                                <TouchableOpacity
                                    style={styles.column}
                                    onPress={
                                        () => this.getManualServiceOrder()
                                    }
                                >
                                    <Image style={styles.icon} source={require('../../assets/manual.png')} />
                                    <Text>Manual</Text>
                                </TouchableOpacity>
                            </Col>

                        </Row>

                        <Row style={styles.row}>
                            <Col style={styles.each}>
                                <TouchableOpacity
                                    style={styles.column}
                                    onPress={
                                        () => this.getAllMaterial()
                                    }
                                >
                                    <Image style={styles.icon} source={require('../../assets/materials.png')} />
                                    <Text>Materials</Text>
                                </TouchableOpacity>
                            </Col>

                            <Col style={styles.each}>
                                <TouchableOpacity
                                    style={styles.column}
                                    onPress={
                                        () => this.getAllTool()
                                    }
                                >
                                    <Image style={styles.icon} source={require('../../assets/tools.png')} />
                                    <Text>Tools</Text>
                                </TouchableOpacity>
                            </Col>


                        </Row>

                        <Row style={styles.row}>
                            <Col style={styles.each} >
                                <TouchableOpacity
                                    style={styles.column}
                                    onPress={
                                        () => this.getAssetsByUserId()
                                    }
                                >
                                    <Image style={styles.icon} source={require('../../assets/assets.png')} />
                                    <Text>Live Data</Text>
                                </TouchableOpacity>
                            </Col>

                            <Col style={styles.each}>
                                <TouchableOpacity
                                    style={styles.column}
                                    onPress={
                                        () => this.goToNotifications()
                                    }
                                >
                                    <Image style={styles.icon} source={require('../../assets/alarms.png')} />
                                    <Text>Notifications</Text>
                                </TouchableOpacity>
                            </Col>

                        </Row>

                        {/*<Row style = {styles.row}>*/}

                        {/*<Col style = {styles.each}>*/}
                        {/*    <TouchableOpacity*/}
                        {/*        style = {[styles.column]}*/}
                        {/*        onPress = {*/}
                        {/*            () => Actions.portfolio()*/}
                        {/*        }*/}
                        {/*    >*/}
                        {/*        <Image style = {styles.icon} source = {require('../../assets/portfolio.png')}/>*/}
                        {/*        <Text>Portfolio </Text>*/}
                        {/*    </TouchableOpacity>*/}
                        {/*</Col>*/}
                        {/*</Row>*/}
                    </Grid>
                </View>
                <View>
                    <TouchableOpacity
                        style={styles.logout}
                        onPress={() => this.logout()}
                    >
                        <Text style={{ color: 'white', fontSize: 16 }}>Welcome {this.state.name},      Log out</Text>
                    </TouchableOpacity>
                    <Footer />
                </View>
            </View>
        )
    }
}
export default Home

//
//import React, { Component } from 'react';
//import { Animated, TouchableWithoutFeedback, Text, View, StyleSheet } from 'react-native';
//
//export default class Home extends Component {
//  constructor(props) {
//    super(props)
//
//    this.moveAnimation = new Animated.Value(0);
//  }
//
//  _moveBall = () => {
//    Animated.timing (this.moveAnimation, {
//      toValue: 100,
//      duration: 5000,
//      useNativeDriver: true,
//
//    }).start()
//
//  }
//
//  render() {
//    return (
//      <View style={styles.container}>
//        <Animated.View style={[styles.tennisBall]}>
//          <TouchableWithoutFeedback style={styles.button} onPress={this._moveBall}>
//            <Text style={styles.buttonText}>Press</Text>
//          </TouchableWithoutFeedback>
//        </Animated.View>
//      </View>
//    );
//  }
//}
//
//const styles = StyleSheet.create({
//  container: {
//    flex: 1,
//    backgroundColor: '#ecf0f1',
//  },
//  tennisBall: {
//    display: 'flex',
//    justifyContent: 'center',
//    alignItems: 'center',
//    backgroundColor: 'greenyellow',
//    borderRadius: 100,
//    width: 100,
//    height: 100,
//  },
//  button: {
//    paddingTop: 24,
//    paddingBottom: 24,
//  },
//  buttonText: {
//    fontSize: 24,
//    color: '#333',
//  }
//});
