import { StyleSheet, Dimensions } from 'react-native'

let { height, width } = Dimensions.get('window')
export default StyleSheet.create({
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
    backgroundStyle: {
        flex: 1,
        width: 500,
        height: 700,
        resizeMode: 'cover'
    },
    imageContainer: {
        height: 70,
        width: 70,
        borderRadius: 35,
        // marginBottom: 40,
        display: "flex",
        justifyContent: "space-around"
    },
    image: {
        height: 70,
        width: 70,
        borderRadius: 35,
        backgroundColor: "white"
    },
    bottomContainer: {
        padding: 20,
        paddingBottom: 30,
        width: "100%",
        display: "flex",
        justifyContent: "space-around",
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "black"
    }
})