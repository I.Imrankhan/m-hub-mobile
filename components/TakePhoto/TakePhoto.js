import React, { Component } from 'react'
import styles from './TakePhotoCss'
import uploadStyles from '../ServiceOrders/ServiceOrderCss'
import { RNCamera } from 'react-native-camera';
import { Image, TouchableOpacity, View, ActivityIndicator, Text } from 'react-native'
import { updateStatus, saveOrderLogPhotoLocally, uploadPmChecklistFile } from '../Network/api/viewServiceApi'
import obj from '../config'

export default class TakePhoto extends Component {
    state = {
        imageUri: "",
        isUploading: false
    }
    takePicture() {
        this.camera.takePictureAsync().then((uri) => {
            this.setState({ imageUri: uri });
            this.camera.pausePreview();
        })
            .catch(err => console.error(err));
    }

    cancel = () => {
        this.setState({ imageUri: "" })
        this.camera.resumePreview()
    }

    submitPhoto = () => {
        if (obj.isConnected) {
            this.setState({
                isUploading: true
            })
            if (this.props.url == "status") {
                updateStatus(this.state.imageUri, { user_hCode: this.props.user_hCode, order_hCode: this.props.order_hCode })
                    .then((response) => {
                        this.setState({
                            isUploading: false
                        })
                        alert(response.message)
                        this.cancel()
                    })
            }
            else {
                uploadPmChecklistFile(this.state.imageUri, this.props.id)
                    .then((response) => {
                        this.setState({
                            isUploading: false
                        })
                        alert(response.message)
                        this.cancel()
                    })
            }
        }
        else {
            saveOrderLogPhotoLocally(this.state.imageUri, { user_hCode: this.props.user_hCode, order_hCode: this.props.order_hCode })
                .then(response => {
                    alert(response)
                })
            this.cancel()
        }
    }

    render() {

        const { imageUri } = this.state;

        return (


            < RNCamera
                ref={ref => {
                    this.camera = ref;
                }
                }
                style={styles.preview}
                type={RNCamera.Constants.Type.back}
            >
                {/* <Text onPress={this.takePicture.bind(this)} >SNAP</Text> */}
                < View style={styles.bottomContainer} >
                    {
                        !imageUri == "" &&
                        <TouchableOpacity onPress={this.cancel}>
                            <Image source={require('../../assets/cancel.png')}></Image>
                        </TouchableOpacity>
                    }
                    {
                        imageUri == "" &&

                        <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.imageContainer}>
                            <Image style={styles.image}
                                source={{ uri: 'http://www.free-avatars.com/data/media/37/cat_avatar_0597.jpg' }}
                            />

                        </TouchableOpacity>
                    }

                    {
                        !imageUri == "" &&
                        <TouchableOpacity onPress={this.submitPhoto}>
                            <Image source={require('../../assets/tick.png')}></Image>
                        </TouchableOpacity>
                    }
                </View >
                {
                    this.state.isUploading &&
                    <View style={uploadStyles.blur}>
                        <View style={uploadStyles.loaderContainer}>
                            <View style={uploadStyles.downloading}>
                                <ActivityIndicator style={{ marginRight: 30, marginLeft: -30 }} size="large" color="#0000ff" />
                                <Text>Uploading...</Text>
                            </View>
                        </View>
                    </View>
                }
            </RNCamera >
        )
    }


}

