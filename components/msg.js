
var admin = require("firebase-admin");

var serviceAccount = require("./mhub_daiva_config.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://nova-fbase.firebaseio.com"
});




var payload = {
    notification: {
      title: "NASDAQ News",
      body: "The NASDAQ climbs for the second day. Closes up 0.60%."
    }
  };
  
  var topic = "finance";
  
  admin.messaging().sendToTopic(topic, payload)
    .then(function(response) {
      console.log("Successfully sent message:", response);
    })
    .catch(function(error) {
      console.log("Error sending message:", error);
    });