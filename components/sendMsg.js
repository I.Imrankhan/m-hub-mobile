XLSX = require('xlsx');
let workbook = XLSX.readFile('../../../../downloads/dgr.xls');
//console.log(workbook.Sheets['Generation Data'].H2.w, workbook.Sheets['Generation Data'].N2.w)
let x = workbook.Sheets['Generation Data'].H2.w
let y = workbook.Sheets['Generation Data'].N2.w

let http = require('http');
let urlencode = require('urlencode');
let msg = urlencode('Your DGR is ' + x + ' and machine availability is ' + y);
let toNumber = '918124966143'
let hash = 'T4uQWfS1CXk-fYphQPqEmgSCpf22GDLlZOhv8fWF7L';
let sender = urlencode('TXTLCL');
let data = 'apikey=' + hash + '&sender=' + sender + '&numbers=' + toNumber + '&message=' + msg;
let options = {
  host: 'api.textlocal.in', path: '/send?' + data
};

callback = function (response) {
  let str = '';
  response.on('data', function (chunk) {
    str += chunk;
  });
  response.on('end', function () {
    console.log(str);
  });
}
http.request(options, callback).end();