import React, { Component } from 'react';
import { View, Image, StyleSheet, Dimensions } from 'react-native'
let {height, width} = Dimensions.get('window');

class NoData extends Component {
   render() {
       return (
            <View style = {styles.container}>
                <Image style = {styles.noDataImage}source = {require('../../assets/nodata.png')} />
            </View>
       );
   }
}
export default NoData

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    noDataImage: {
        marginTop: -(height / 5),
        height: height / 4.7 ,
        width: width / 2
    }
})