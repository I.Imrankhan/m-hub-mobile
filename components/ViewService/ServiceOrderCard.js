import React, { Component } from 'react'
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    Dimensions,
    Linking,
} from 'react-native'
import styles from './viewServiceCss'
import assetCss from '../Assets/assetCss';

const { width, height } = Dimensions.get('window');



export default SeviceOrderCard = (props) => {
    let assets = ''
    let data = {...props}
    if (typeof data.item.asset == "object") {
        data.item.asset.map((ass, index) => {
            assets += ass.asset
            if (index < data.item.asset.length - 1) {
                assets += ','
            }
        })
        data.item.asset = assets
    }
    return (

        <View style={{ alignItems: 'center' }}>
            <View style={[styles.card, styles.margin]}>
                {
                    data.keys.map((key, i) => (
                        <View style={styles.row} key={i}>
                            <Text style={styles.title}>{data.title[i]}</Text>
                            <Text style={styles.text}>{data.item[key]}</Text>
                        </View>
                    ))
                }
            </View>

            {
                data.service == 'Prognosis' &&
                <View>
                    <Image
                        style={{ height: 300, width: width, marginBottom: 30 }}
                        source={{ uri: data.item.link }}
                    />
                </View>

            }
        </View>

    )
}

export const CheckListCard = (props) => {
    let checkedListArr = props.checkedListArr
    let checkedListData;
    return (
        <View style={[styles.checkListContainer, styles.margin]}>
            {
                props.checkListData.map((checkList, checklistIndex) => (
                    <View style={[styles.card, styles.margin, styles.checkList]} key={checklistIndex}>
                        <View style={styles.row}>
                            <Text style={styles.checkListTitle}>{checklistIndex + 1}</Text>
                            <Text style={styles.checkListText}>{checkList.checklist}</Text>
                        </View>
                        <View style={[styles.row, styles.rowCenter]}>
                            <Text style={styles.checkListTitle}>Status</Text>
                            <CheckBox
                                value={props.checkedListArr[checklistIndex]}
                                onValueChange={(value) => {
                                    arr = checkedListArr
                                    arr[checklistIndex] = !arr[checklistIndex]
                                    checkedListArr = arr
                                }}
                            />
                        </View>

                        {
                            checkList.comments.length > 0 &&
                            <View>
                                <View style={styles.row}>
                                    <Text style={styles.checkListTitle} >Comments</Text>
                                </View>
                                <View style={[styles.row, styles.rowCenter]}>
                                    <TextInput
                                        value={checkList[checkListKeys[1]]}
                                        onChangeText={(value) => {
                                            copyData = Object.assign({}, props.checkListData);
                                            copyData[checklistIndex].comments = value;
                                            checkedListData = copyData
                                        }}
                                        style={styles.input}
                                    />
                                </View>
                            </View>
                        }
                    </View>
                ))
            }
            <TouchableOpacity
                style={styles.submitButton}
                onPress={
                    () => this.saveCheckList()
                }
            >
                <Text style={styles.submitButtonText}>Save</Text>
            </TouchableOpacity>
        </View>
    )
}


export const OrderLogs = (props) => {
    let logs = props.orderLogs.reverse()
    let comments = props.comments.reverse()
    return (
        logs.map((log, index) => (
            <View style={[styles.card, styles.margin, styles.checkList]} key={index}>
                <View style={styles.row}>
                    <Text style={styles.checkListTitle}>Comments</Text>
                    <Text style={styles.checkListText}>
                        {comments[index]}
                    </Text>
                </View>

                <View style={styles.row}>
                    <Text style={styles.checkListTitle}>Status</Text>
                    <Text style={styles.checkListText}>
                        {log.sname}
                    </Text>
                </View>

                <View style={styles.row}>
                    <Text style={styles.checkListTitle}>Created On</Text>
                    <Text style={styles.checkListText}>
                        {log.created_on}
                    </Text>
                </View>

                <View style={styles.row}>
                    <Text style={styles.checkListTitle}>File</Text>
                    {

                        log.files.length == 0 &&
                        <Text style={styles.checkListText}>
                            No File
                        </Text>
                    }
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        {
                            log.files.map((file, fileIndex) => (
                                <TouchableOpacity style={{ flex: 1, flexDirection: 'row', marginBottom: 15 }} key={fileIndex} >
                                    <Text style={{ marginRight: 10 }}>{fileIndex + 1}.</Text>
                                    <Text
                                        style={{
                                            color: '#0056b3',
                                            marginTop: -1,
                                            flex: 1,
                                            flexWrap: 'wrap'
                                        }}
                                        onPress={() => Linking.openURL(file.path)}
                                    >
                                        {file.filename}
                                    </Text>
                                </TouchableOpacity>
                            ))
                        }
                    </View>

                </View>

            </View>
        ))
    )
}