import React, { Component } from 'react'
import {
    View,
    ScrollView,
    Image,
    Text,
    TouchableOpacity,
    ActivityIndicator,
    CheckBox,
    TextInput,
    Picker,
    Dimensions,
    AsyncStorage,
    Linking,
    RefreshControl

} from 'react-native'
import Textarea from 'react-native-textarea';
import obj from '../config.js'
import styles from './viewServiceCss.js'
import OfflineNotice from '../OfflineNotice/OfflineNotice.js'
import SaveLocal from '../Network/save/SaveLocal.js'
import ServiceOrderCard, { OrderLogs } from './ServiceOrderCard'
import {
    fetchPunchPoints,
    fetchCheckList,
    fetchBundledActivities,
    fetchServiceOrderStatus,
    fetchOrderLogsData,
    getPrognosisChecklist,
    uploadLocallySavedFiles,
    deletePmFile
} from '../Network/api/viewServiceApi'
import { Actions } from 'react-native-router-flux';
import uploadStyles from '../ServiceOrders/ServiceOrderCss'

class ViewServiceOrder extends Component {
    constructor(props) {
        super(props)
        saveObj = new SaveLocal();
    }
    win = Dimensions.get('window');

    images = [{
        url: 'https://avatars2.githubusercontent.com/u/7970947?v=3&s=460',
    }]
    state = {
        serviceOrderData: [],
        status: true,
        dataNotFound: false,
        checkedListArr: [],
        bundledListArr: [],
        statusData: [],
        orderLogs: [],
        checkListData: [],
        bundledActivitiesData: [],
        comments: '',
        val: '',
        checkListComments: '',
        punchPoints: [],
        asyncData: [],
        count: 0,
        isLoading: true,
        refreshing: false,
        statusId: 1,
        isSaving: false,
        savingText: "Saving...",
        showDelete: false,
        left: 0,
        top: 0,
        // showChecklistComments: false
    }

    plant;
    serArr = [];
    _onRefresh = () => {
        this.setState({ refreshing: true });
        this.componentDidMount().then(() => {
            this.setState({ refreshing: false });
        });
    }


    async componentDidMount() {
        this.props.navigation.setParams({
            title: "View " + this.props.service
        })
        data = []
        if (obj.isConnected) {
            await AsyncStorage.getItem('serOrderFiles').then(async (data) => {
                if (data) {
                    this.setState({
                        isSaving: true,
                        savingText: "Saving Offline Data..."
                    })
                    let res = await uploadLocallySavedFiles(data)
                    if (res) {
                        this.setState({
                            isSaving: false,
                            savingText: "Saving..."
                        })
                    }
                }
            })
            if (this.props.service == 'Manual') {
                await AsyncStorage.getItem('manual').then((value) => {
                    if (value != null) {
                        this.setState({
                            asyncData: JSON.parse(value).manualServiceDetails
                        })
                    }
                })
            }


            else {
                await AsyncStorage.getItem('prognosis').then((value) => {
                    if (value != null) {
                        this.setState({
                            asyncData: JSON.parse(value).prognosisServiceDetails
                        })
                    }
                })
            }

            if (this.state.asyncData.length && Object.keys(this.state.asyncData[0]).includes(this.props.plant)) {
                this.state.asyncData[0][this.props.plant].filter(this.filterServiceOrder)

                if (this.serArr[0]) {
                    if (this.serArr[0].bundled_activities.length) {
                        if (Object.keys(this.serArr[0].bundled_activities[0]).includes('isChanged')) {
                            if (this.serArr[0].bundled_activities[0].isChanged)
                                this.saveBundledActivities(this.serArr[0].bundled_activities)
                        }
                    }


                    if (this.serArr[0].checklist.length) {
                        if (Object.keys(this.serArr[0].checklist[0]).includes('isChanged')) {
                            if (this.serArr[0].checklist[0].isChanged)
                                this.saveCheckList(this.serArr[0].checklist)
                        }
                    }


                    if (this.serArr[0].Download_Files.length) {
                        this.serArr[0].Download_Files.map((item, index) => {
                            if (item.isAdded) {
                                this.saveServiceStatus(item.comments)

                            }
                        })
                    }

                    if (this.serArr[0].punch_points.length) {
                        this.serArr[0].punch_points.map((item, index) => {
                            if (item.isChanged)
                                this.updatePunchPoints(index, item)
                        })
                    }
                }
            }

            fetch(obj.BASE_URL + "api/controlCenter/getServiceOrderListById/" + this.props.order_hCode)
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log(responseJson.data)
                    if (!responseJson.data.length) {
                        this.setState({
                            dataNotFound: true
                        })
                    }

                    else {
                        if (this.props.service == "Prognosis") {
                            responseJson.data[0].asset = this.props.asset
                        }
                        this.setState({
                            serviceOrderData: responseJson.data
                        })
                    }
                    if (this.props.service == 'Prognosis' && obj.isConnected && responseJson.data.length) {
                        this.fetchCheckList()
                    }
                    else {
                        if (obj.isConnected) {
                            this.fetchBundledActivities()
                            this.fetchPunchPoints()
                        }
                    }
                    if (obj.isConnected) {
                        this.fetchServiceOrderStatus()
                        this.fetchOrderLogsData()
                    }

                })
                .catch((error) => {
                    // alert(error);
                });


        }

        else {
            let checkOrBundle;
            let serviceDetails = {};
            let serviceType;
            if (this.props.service == 'Manual') {
                await AsyncStorage.getItem('manual').then((value) => {
                    if (value) {
                        this.setState({
                            asyncData: JSON.parse(value).manualServiceDetails,
                            statusData: JSON.parse(value).statusData,
                            offLineDateFound: true
                        })
                    }
                    else {
                        this.setState({
                            offLineDateFound: false,
                            isLoading: false
                        })
                    }
                })
            }

            else {
                await AsyncStorage.getItem('prognosis').then((value) => {
                    // console.log(JSON.parse(value))
                    if (value) {
                        // console.log('prognosisServiceDetails')
                        // console.log(JSON.parse(value))
                        this.setState({
                            asyncData: JSON.parse(value).prognosisServiceDetails,
                            statusData: JSON.parse(value).statusData,
                            offLineDateFound: true,
                        })
                    }
                    else {
                        this.setState({
                            offLineDateFound: false,
                            isLoading: false
                        })
                    }
                })
            }

            this.plant = this.props.plant
            let index = this.props.index

            if (this.state.offLineDateFound) {

                this.state.asyncData[0][this.plant].filter(this.filterServiceOrder)
                this.serArr[0]['asset'] = this.props.asset
                this.serArr[0]['link'] = this.serArr[0].file_name

                this.makeCheckedList(this.serArr[0].checklist)
                if (this.props.service == 'Manual') {
                    this.makeBundledList(this.serArr[0].bundled_activities)
                    this.makePunchList(this.serArr[0].punch_points)
                }
                this.setState({
                    //                    id: this.state.asyncData[0][this.plant][index].sub_comp_id_fk,
                    serviceOrderData: this.serArr,
                    //                    checked: base.checked,
                    isLoading: false,
                    orderLogs: this.serArr[0].Download_Files,
                    checkListData: this.serArr[0].checklist,
                    bundledActivitiesData: this.serArr[0].bundled_activities,
                    punchPoints: this.serArr[0].punch_points
                })
            }

        }

    }

    filterServiceOrder = (serviceOrder) => {
        this.setState({
            count: this.state.count + 1
        })
        let serNumArr = serviceOrder.ser_order_num.split('-')
        let ser_order_num = parseInt(serNumArr[serNumArr.length - 1])
        if (ser_order_num == this.props.order_id) {
            this.serArr[0] = serviceOrder
            // if(!obj.isConnected) {
            //     this.setState({
            //          isLoading: false
            //     })
            // }
        }
        //        if(this.props.service == 'Manual')
        //            return parseInt(serviceOrder.ser_order_num.split('-')[3]) == (this.props.order_id)
        //        else
        //            return parseInt(serviceOrder.log_id) == this.props.order_id
    }

    fetchPunchPoints = () => {
        fetchPunchPoints(this.props.order_hCode).then((data) => {
            this.setState({
                punchPoints: data
            })
        })
    }



    fetchCheckList = () => {

        let title = this.state.serviceOrderData[0].title
        fetchCheckList(this.props.order_hCode).then((data) => {
            // console.log("Check List")
            if (data.length) {
                this.makeCheckedList(data)
                this.setState({
                    checkListData: data
                })
            }
            else {
                getPrognosisChecklist(title).then((progChecklistData) => {
                    this.setState({
                        checkListData: progChecklistData
                    })
                })
            }

        })
    }

    fetchBundledActivities = () => {
        fetchBundledActivities(this.props.order_hCode, this.props.pm_type_id_fk).then((data) => {
            this.makeBundledList(data)
            // this.setState({
            //     bundledActivitiesData: data
            // })
            if (data.length) {
                data.map(item => {
                    item.files.map(file => {
                        file.fname = file.url.split('//')[1]
                    })
                    if (!item.comments || item.comments == 'null') {
                        item.comments = ""
                    }
                })
                this.setState({
                    bundledActivitiesData: data
                })
            }
        })
    }

    makeCheckedList = (data) => {
        //        alert('val = '+ data[0].isCompleted+' '+data[1].isCompleted)
        let arr = []
        data.map((item) => {
            //            alert('item = '+ item.isCompleted)
            if (item.isCompleted == "1")
                arr.push(true)
            else
                arr.push(false)
        })

        //        alert(arr[0]+' '+arr[1])

        this.setState({
            checkedListArr: arr
        })
    }

    makePunchList = (data) => {

        let arr = []
        data.map((item) => {
            //            console.log('in map')
            if (item.status == "1")
                arr.push(true)
            else
                arr.push(false)
        })

        this.setState({
            punchListArr: arr
        })
    }

    makeBundledList = (data) => {
        let arr = []
        data.map((item) => {
            if (item.astatus) {
                arr.push(true)
            }
            else
                arr.push(false)

        })

        this.setState({
            bundledListArr: arr
        })
    }

    fetchServiceOrderStatus = () => {
        fetchServiceOrderStatus().then((data) => {
            this.setState({
                statusData: data
            })
        })
    }

    fetchOrderLogsData = () => {
        fetchOrderLogsData(this.props.order_hCode).then((data) => {
            this.setState({
                orderLogs: data,
                isLoading: false
            })
        })
    }

    saveCheckList = (offData) => {

        let data = []
        let checked = []
        if (offData != undefined) {
            if (offData.length) {
                data = offData
                data.map((item, index) => {
                    if (item.isCompleted == '1')
                        checked[index] = 1
                    else
                        checked[index] = 0
                })
            }
        }

        else {
            data = this.state.checkListData
            checked = this.state.checkedListArr
        }

        let i;

        if (this.state.asyncData.length) {
            this.state.asyncData[0][this.props.plant].map((item, index) => {
                if (item.ser_order_num == this.serArr[0].ser_order_num) {
                    i = index
                }
            })
        }
        url = ''
        data.map((item, index) => {
            url = 'mskSaveChecklist'
            if (checked[index]) {
                item['isComplete'] = "1"
                item.isCompleted = 1
            }
            else
                item['isCompleted'] = "0"

        })

        if (obj.isConnected) {
            data.map(item => {
                item.checklist_id_fk = item.id
            })
            fetch(obj.BASE_URL + "api/controlCenter/" + url, {
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/json',
                }),
                body: JSON.stringify({
                    "user_hcode": this.props.user_hCode,
                    "order_hcode": this.props.order_hCode,
                    "checkListArray": data
                })
            })
                .then((response) => response.text())
                .then((responseText) => {
                    responseObj = JSON.parse(responseText)
                    alert(responseObj.message)
                    if (offData == undefined) {
                        alert(responseObj.message)
                        saveObj.setIsChanged(
                            this.props.service, this.props.plant, this.state.checkListData, i, 'checklist', 1
                        )
                    }

                    else
                        saveObj.setIsChanged(
                            this.props.service, this.props.plant, this.state.checkListData, i, 'checklist'
                        )
                    if (this.state.serviceOrderData.length) {
                        this.fetchCheckList()
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }
        else {
            saveObj.saveCheckListLocally(this.props.service, this.props.plant, i, this.state.checkListData)
        }
    }


    saveBundledActivities = (offData) => {
        let data = []
        let checked = []
        if (offData != undefined) {
            if (offData) {
                data = offData
                data.map((item, index) => {
                    if (item.astatus)
                        checked[index] = 1
                    else
                        checked[index] = 0
                })
            }
        }

        else {
            data = this.state.bundledActivitiesData
            checked = this.state.bundledListArr
        }

        let i;
        if (this.state.asyncData.length) {
            // console.log(this.serArr[0].ser_order_num)
            this.state.asyncData[0][this.props.plant].map((item, index) => {
                if (item.ser_order_num == this.serArr[0].ser_order_num) {
                    i = index
                }
            })
        }

        url = ''
        let save = 1;
        for (let i = 0; i < data.length; i++) {
            url = 'SaveBundledActivity'
            if (checked[i])
                data[i]['astatus'] = 1
            else if(!data[i].files.length) {
                alert(`If "Is ok" is not enabled then it is neccessary to upload a file for that checklist`);
                save = 0;
                this.fetchBundledActivities()
                break;
            }
            else {
                data[i]['astatus'] = 0
            }
        
        }

        
        console.log(data)
        if (obj.isConnected && save) {
            this.setState({
                isSaving: true
            })
            fetch(obj.BASE_URL + "api/controlCenter/" + url, {
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/json',
                }),
                body: JSON.stringify({
                    "user_hcode": this.props.user_hCode,
                    "order_hcode": this.props.order_hCode,
                    "checkListArray": data
                })
            })

                .then((response) => response.text())
                .then((responseText) => {
                    this.setState({
                        isSaving: false
                    })
                    responseObj = JSON.parse(responseText)
                    if (offData == undefined) {
                        alert(responseObj.message)
                        // saveObj.setIsChanged(
                        //     this.props.service, this.props.plant, this.state.bundledActivitiesData, i, 'bundled', 1
                        // )
                    }

                    else {
                        saveObj.setIsChanged(
                            this.props.service, this.props.plant, this.state.bundledActivitiesData, i, 'bundled'
                        )
                    }
                    this.fetchBundledActivities()
                })
                .catch((error) => {
                    console.error(error);
                });
        }

        // else {
        //     saveObj.saveBundledLocally(this.props.plant, i, this.state.bundledActivitiesData)
        // }
    }



    shouldComponentUpdate() {
        //        alert('shouldComponentUpdate')
        return true
    }

    saveServiceStatus = (statusComment) => {
        let comment

        if (obj.isConnected && statusComment != undefined) {
            if (statusComment.length)
                comment = statusComment
        }
        else
            comment = this.state.comments
        if (obj.isConnected) {
            fetch(obj.BASE_URL + "api/controlCenter/updateStatus", {
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/json',
                }),
                body: JSON.stringify({
                    "severity_id_fk": "1",
                    "user_hcode": this.props.user_hCode,
                    "order_hcode": this.props.order_hCode,
                    "comments": comment,
                    "status_id_fk": this.state.statusId,
                })
            })

                .then((response) => response.text())
                .then((responseText) => {
                    responseObj = JSON.parse(responseText)
                    //                if(statusComment != undefined) {
                    //                    alert(responseObj.message)
                    //                }
                    if (statusComment == undefined) {
                        alert(responseObj.message)
                    }

                    this.setState({
                        comments: ""
                    })
                    this.fetchOrderLogsData()
                    saveObj.setIsAdded(
                        this.props.service, this.state.asyncData, this.props.plant, this.serArr
                    )
                })
                .catch((error) => {
                    console.error(error);
                });
        }

        else {
            this.state.asyncData[0][this.props.plant].map((item, index) => {
                if (item.ser_order_num == this.serArr[0].ser_order_num) {
                    i = index
                }
            })
            saveObj.saveDownloadFilesLocally(this.props.service, this.props.plant, i, this.state.comments)
        }
    }



    handleComments = (text) => {
        this.setState({
            comments: text
        })
    }

    updatePunchPoints = (index, offData) => {
        //        console.log(offData)
        let data = []
        let checked = []
        let showMsg = false;

        if (offData != undefined) {
            data = offData
            if (data.status == '1')
                checked[index] = 1
            else
                checked[index] = 0
        }

        else {
            showMsg = true
            data = this.state.punchPoints[index]
            checked = this.state.punchListArr
        }

        //        console.log('punch list array')
        //        console.log(this.state.punchListArr)

        if (checked[index])
            data.status = "1"
        else
            data.status = "2"

        let punchPointArr = [{
            "punch_id_fk": data.punch_id_fk,
            "resp": data.resp,
            "status": data.status
        }]

        if (obj.isConnected) {
            fetch(obj.BASE_URL + "api/controlCenter/savePunchPointResp", {
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/json',
                }),

                body: JSON.stringify({
                    "user_hcode": obj.user_hCode,
                    "order_hcode": this.props.order_hCode,
                    "punchpointArray": punchPointArr
                })
            })
                .then((response) => response.text())
                .then((responseText) => {
                    responseObj = JSON.parse(responseText)
                    if (showMsg) {
                        alert(responseObj.message)
                        this.setPunchPointIsChanged()
                    }
                    this.fetchPunchPoints()
                })
                .catch((error) => {
                    console.log(error);
                });
        }


        else {
            let i;
            this.state.asyncData[0][this.props.plant].map((item, index) => {
                if (item.ser_order_num == this.serArr[0].ser_order_num) {
                    i = index
                }
            })
            AsyncStorage.getItem('manual')
                .then(JSONData => {
                    JSONData = JSON.parse(JSONData);
                    JSONData.manualServiceDetails[0][this.props.plant][i].punch_points = this.state.punchPoints
                    JSONData.manualServiceDetails[0][this.props.plant][i].punch_points[index]['isChanged'] = 1
                    AsyncStorage.setItem('manual', JSON.stringify(JSONData))
                    alert('Updated successfully')
                }).done();
        }
    }

    setPunchPointIsChanged = () => {
        let i;
        this.state.asyncData[0][this.props.plant].map((item, index) => {
            if (item.ser_order_num == this.serArr[0].ser_order_num) {
                i = index
            }
        })

        AsyncStorage.getItem('manual')
            .then(JSONData => {
                JSONData = JSON.parse(JSONData);
                let punch_points = JSONData.manualServiceDetails[0][this.props.plant][i].punch_points
                punch_points.map((item) => {
                    item.isChanged = 0
                })
                JSONData.manualServiceDetails[0][this.props.plant][i].punch_points = this.state.punchPoints
                AsyncStorage.setItem('manual', JSON.stringify(JSONData))
            })
    }

    takePhoto = (url, id) => {
        Actions.takePhoto({ user_hCode: this.props.user_hCode, order_hCode: this.props.order_hCode, url, id })
    }

    handlePmChecklistChange = (value, index) => {
        let newState = { ...this.state }
        newState.bundledActivitiesData[index].comments = value
        this.setState({ newState })
        // console.log(this.state.bundledActivitiesData)
    }

    deletePmChecklistFile = (id) => {
        let scope = this;
        deletePmFile(id)
            .then(res => {
                alert(res.message)
                scope.fetchBundledActivities()
            })
    }

    showDeleteOption = (e) => {
        console.log(e.nativeEvent)
        this.setState({
            showDelete: true,
            left: e.nativeEvent.pageX - 20,
            top: e.nativeEvent.pageY
        })
    }

    render() {
        //        alert('render')

        progcheckListData = this.state.checkListData
        progcheckListData.map(item => {
            if (!item.comments) {
                item.comments = ""
            }
        })

        comments = []
        checkListKeys = []
        checkListText = ''
        keys = []
        title = []
        if (this.props.service == 'Prognosis') {
            keys = ['ser_order_num', 'title', 'plant_name', 'asset', 'tname', 'cname', 'scname', 'iname', 'sname']
            title = ['Service Id', 'Title', 'Plant Name', 'Asset Name', 'Trigger Type', 'Component', 'Sub Component', 'Indicator', 'Severity']
            checkListKeys = ['checklist', 'comments']
            showComments = true
            checkListText = 'Check List'
        }

        else {
            keys = ['ser_order_num', 'plant_name', 'asset', 'title', 'tname', 'cname', 'scname', 'iname', 'sname', 'comments',
                'created_on'
            ]
            title = ['Service Number', 'Plant Name', 'Asset', 'Title', 'Trigger Type', 'Component', 'Sub Component',
                'Indicator', 'Severity', 'Comments', 'Created On'
            ],
                checkListKeys = ['aname', 'comments']
            showComments = false
            checkListText = 'Bundled Activities'
        }
        checkListData = false
        disable = true;
        checkListDisable = true
        isPunchPoints = false

        if (this.state.checkListData.length)
            checkListData = true

        if (this.state.comments.length)
            disable = false

        if (this.state.checkListComments.length)
            checkListDisable = false


        //        if(this.state.dataNotFound) {
        //            return(
        //                <NoData />
        //            )
        //        }

        if (this.state.isLoading) {
            return (
                <View style={styles.horizontal} >
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        }

        if (this.state.punchPoints.length)
            isPunchPoints = true

        // this.state.orderLogs.map((item) => (
        //     (item.comments == 'null' || item.comments.length == 0) && comments.push('-'),
        //     (item.comments != 'null' && item.comments.length) && comments.push(item.comments)
        // ))

        for (let i = 0; i < this.state.orderLogs.length; i++) {
            if (this.state.orderLogs[i].comments) {
                comments.push(this.state.orderLogs[i].comments)
            }
            else {
                comments.push('-')
            }
        }


        return (
            <View>
                <OfflineNotice />
                <ScrollView
                    keyboardShouldPersistTaps={'handled'}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                            colors={['#007bff']}
                        />
                    }
                >
                    <View style={styles.container}>
                        {
                            this.state.serviceOrderData.length != 0 &&
                            this.state.serviceOrderData.map((item, index) => (
                                index == 0 &&
                                <ServiceOrderCard
                                    title={title}
                                    keys={keys}
                                    item={item}
                                    key={index}
                                    service={this.props.service}
                                />

                            ))
                        }

                        {
                            checkListData &&
                            <View>
                                <View style={styles.header}>
                                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                                        Check List
                                    </Text>
                                </View>
                                <View style={[styles.checkListContainer, styles.margin]}>
                                    {
                                        this.state.checkListData.map((checkList, checklistIndex) => (
                                            <View style={[styles.card, styles.margin, styles.checkList]} key={checklistIndex}>
                                                <View style={styles.row}>
                                                    <Text style={styles.checkListTitle}>{checklistIndex + 1}</Text>
                                                    <Text style={styles.checkListText}>{checkList.checklist}</Text>
                                                </View>
                                                <View style={[styles.row, styles.rowCenter]}>
                                                    <Text style={styles.checkListTitle}>Status</Text>
                                                    <CheckBox
                                                        value={this.state.checkedListArr[checklistIndex]}
                                                        onValueChange={(value) => {
                                                            arr = this.state.checkedListArr
                                                            arr[checklistIndex] = !arr[checklistIndex]
                                                            this.setState({
                                                                checkedListArr: arr
                                                            })
                                                        }}
                                                    />
                                                </View>

                                                {
                                                    // Object.keys(checkList).includes('comments') &&

                                                    <View>
                                                        <View style={styles.row}>
                                                            <Text style={styles.checkListTitle} >Comments</Text>
                                                        </View>
                                                        <View style={[styles.row, styles.rowCenter]}>
                                                            <TextInput
                                                                value={checkList.comments}
                                                                onChangeText={(value) => {
                                                                    copyData = Object.assign({}, this.state);
                                                                    copyData.checkListData[checklistIndex].comments = value;
                                                                    this.setState({
                                                                        checkListData: copyData.checkListData
                                                                    })
                                                                }}
                                                                style={styles.input}
                                                            />
                                                        </View>
                                                    </View>
                                                }
                                            </View>
                                        ))
                                    }
                                    <TouchableOpacity
                                        style={styles.submitButton}
                                        onPress={
                                            () => this.saveCheckList()
                                        }
                                    >
                                        <Text style={styles.submitButtonText}>Save</Text>
                                    </TouchableOpacity>
                                </View>


                            </View>
                        }

                        {
                            this.state.bundledActivitiesData.length > 0 &&
                            <View style={{ zIndex: 1 }}>
                                <View style={styles.header}>
                                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                                        PM Check List
                                    </Text>
                                </View>
                                <View style={[styles.checkListContainer, styles.margin]}>
                                    {/* {
                                        this.state.showDelete &&
                                        <View style={{ padding: 10, position: "absolute", zIndex: 999, left: this.state.left, top: this.state.top }}>
                                            <Text style={{ fontSize: 17, fontWeight: "bold" }}>Delete</Text>
                                        </View>
                                    } */}
                                    {
                                        this.state.bundledActivitiesData.map((checkList, checklistIndex) => (
                                            <View style={[styles.card, styles.margin, styles.checkList, { zIndex: 0 }]} key={checklistIndex}>
                                                <View style={styles.row}>
                                                    <Text style={styles.checkListTitle}>{checklistIndex + 1}</Text>
                                                    <Text style={styles.checkListText}>{checkList['aname']}</Text>
                                                </View>
                                                <View style={[styles.row, styles.rowCenter]}>
                                                    <Text style={styles.checkListTitle}>Is Ok</Text>
                                                    <CheckBox
                                                        value={this.state.bundledListArr[checklistIndex]}
                                                        onValueChange={(value) => {
                                                            arr = this.state.bundledListArr
                                                            arr[checklistIndex] = !arr[checklistIndex]
                                                            this.setState({
                                                                bundledListArr: arr
                                                            })
                                                        }}
                                                    />
                                                </View>
                                                <View>
                                                    <View style={styles.row}>
                                                        <Text style={styles.checkListTitle} >Comments</Text>
                                                    </View>
                                                    <View>
                                                        <TextInput
                                                            value={checkList.comments}
                                                            onChangeText={(value) => {
                                                                this.handlePmChecklistChange(value, checklistIndex)
                                                            }}
                                                            style={styles.input}
                                                        />
                                                    </View>
                                                </View>
                                                {
                                                    checkList.files.length > 0 &&
                                                    <View style={{ marginTop: 10 }}>

                                                        <Text style={{ fontWeight: 'bold', marginLeft: 7, marginBottom: 10 }}>Files : </Text>
                                                        {
                                                            checkList.files.map((file, fileIndex) => (
                                                                <View key={fileIndex} style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                                                                    <TouchableOpacity>
                                                                        <Text
                                                                            key={fileIndex}
                                                                            style={[styles.fileName, {
                                                                                color: '#0056b3',
                                                                                marginTop: -1,
                                                                                marginBottom: 15,
                                                                                marginLeft: 7,
                                                                                fontSize: 15
                                                                            }]}
                                                                            onPress={() => Linking.openURL(file.url)}
                                                                        >
                                                                            {fileIndex + 1}.
                                                                        {
                                                                                ((file.fname).length > 33) ?
                                                                                    (((file.fname).substring(0, 30 - 3)) + '...') :
                                                                                    file.fname

                                                                            }
                                                                        </Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity
                                                                        onPress={() => this.deletePmChecklistFile(file.id)}
                                                                        style={{ marginTop: -3 }}>
                                                                        <Image style={{ width: 25, height: 25 }} source={require('../../assets/trash.png')}></Image>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            ))
                                                        }
                                                    </View>
                                                }
                                                <TouchableOpacity
                                                    onPress={() => this.takePhoto("pmChecklist", checkList.bactivity_id_fk)
                                                    }
                                                    style={[styles.upload, {
                                                        position: "absolute", width: 50,
                                                        top: "10%", right: 10
                                                    }]}>
                                                    <Image style={{ width: 27, height: 27, marginRight: "27%" }} source={require('../../assets/upload.png')}></Image>
                                                    {/* <Text style={{ color: "white" }}>Upload Photo</Text> */}
                                                </TouchableOpacity>
                                            </View>
                                        ))
                                    }
                                    <TouchableOpacity
                                        style={styles.submitButton}
                                        onPress={
                                            () => this.saveBundledActivities()
                                        }
                                    >
                                        <Text style={styles.submitButtonText}>Save</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        }

                        {
                            this.state.punchPoints.length > 0 && this.props.service == 'Manual' &&
                            <View>
                                <View style={styles.header}>
                                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                                        Punch Points
                                </Text>
                                </View>

                                <View style={[styles.checkListContainer, styles.margin]}>
                                    {
                                        this.state.punchPoints.map((point, index) => (
                                            <View style={[styles.card, styles.margin, styles.checkList]} key={index}>
                                                <View style={styles.row}>
                                                    <Text style={styles.checkListTitle}>Point</Text>
                                                    <Text style={styles.checkListText}>{point.points}</Text>
                                                </View>

                                                <View style={[styles.row, styles.rowCenter]}>
                                                    <Text style={styles.checkListTitle}>Status</Text>
                                                    <CheckBox
                                                        value={this.state.punchListArr[index]}
                                                        onValueChange={(value) => {
                                                            arr = this.state.punchListArr
                                                            arr[index] = !arr[index]
                                                            this.setState({
                                                                punchListArr: arr
                                                            })
                                                        }}
                                                    />
                                                </View>
                                                <View>
                                                    <View style={styles.row}>
                                                        <Text style={styles.checkListTitle} >Comments</Text>
                                                    </View>

                                                    <View style={[styles.row, styles.rowCenter]}>
                                                        <TextInput
                                                            value={point.resp}
                                                            onChangeText={(value) => {
                                                                copyData = Object.assign({}, this.state);
                                                                copyData.punchPoints[index].resp = value;
                                                                this.setState({
                                                                    punchPoints: copyData.punchPoints
                                                                })
                                                            }}
                                                            style={styles.input}
                                                        />
                                                    </View>
                                                </View>

                                                {
                                                    point.files.length != 0 &&
                                                    <View>
                                                        <Text style={{ fontWeight: 'bold', marginLeft: 7, marginBottom: 10 }}>Files : </Text>
                                                        {
                                                            point.files.map((file, fileIndex) => (
                                                                <Text
                                                                    key={fileIndex}
                                                                    style={{
                                                                        color: '#0056b3',
                                                                        marginTop: -1,
                                                                        marginBottom: 15,
                                                                        marginLeft: 7
                                                                    }}
                                                                    onPress={() => Linking.openURL(file.link)}
                                                                >
                                                                    {fileIndex + 1}. {file.fpath}
                                                                </Text>
                                                            ))
                                                        }
                                                    </View>
                                                }
                                                <TouchableOpacity
                                                    style={[styles.submitButton, styles.updatePunch]}
                                                    onPress={
                                                        () => this.updatePunchPoints(index)
                                                    }
                                                >
                                                    <Text style={styles.submitButtonText}>Update</Text>
                                                </TouchableOpacity>
                                            </View>
                                        ))
                                    }

                                </View>
                            </View>
                        }

                        {
                            this.props.isAccepted &&
                            <TouchableOpacity
                                onPress={() => this.takePhoto("status")
                                }
                                style={[styles.row, styles.upload]}>
                                <Image style={{ width: 30, height: 30, marginRight: "27%" }} source={require('../../assets/upload.png')}></Image>
                                <Text style={{ color: "white" }}>Upload Photo</Text>
                            </TouchableOpacity>
                        }

                        {
                            // this.state.offLineDateFound &&
                            this.props.isAccepted &&
                            <View>
                                <View style={{ width: 220, padding: 10, backgroundColor: 'white' }}>
                                    <View>
                                        <Text style={{ width: 350, fontSize: 18, fontWeight: 'bold' }}>
                                            Service Status Update
                                            </Text>
                                    </View>
                                </View>

                                <View style={[styles.checkListContainer, styles.margin, { borderRadius: 5 }]}>
                                    <View style={styles.row}>
                                        <Text style={styles.title}>
                                            comments
                                            </Text>
                                    </View>
                                    <View style={styles.row}>
                                        <Textarea
                                            value={this.state.comments}
                                            containerStyle={styles.textareaContainer}
                                            style={styles.textarea}
                                            onChangeText={this.handleComments}
                                            placeholder={'Comments'}
                                            placeholderTextColor={'#c7c7c7'}
                                            underlineColorAndroid={'transparent'}
                                        />
                                    </View>

                                    <View style={styles.row}>
                                        <Picker
                                            selectedValue={this.state.language}
                                            style={{ height: 50, width: '100%' }}
                                            onValueChange={(itemValue, itemIndex) =>
                                                this.setState({ statusId: itemIndex + 1, language: itemValue })
                                            }>
                                            {
                                                this.state.statusData.map((item, index) => (
                                                    <Picker.Item key={index} label={item.sname} value={item.sname} />
                                                ))
                                            }
                                        </Picker>
                                    </View>

                                    <TouchableOpacity
                                        disabled={disable}
                                        style={
                                            [styles.submitButton,
                                            disable ? styles.buttonDisabled : styles.buttonEnabled, { marginTop: 20 }
                                            ]
                                        }
                                        onPress={
                                            () => this.saveServiceStatus()
                                        }
                                    >
                                        <Text style={styles.submitButtonText}>Save</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        }
                        {
                            this.state.orderLogs.length != 0 &&
                            <View>
                                <View style={{ width: 190, padding: 10, backgroundColor: 'white' }}>
                                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Service Order Logs</Text>
                                </View>
                                <View style={[styles.checkListContainer, styles.margin]}>
                                    <OrderLogs
                                        orderLogs={this.state.orderLogs}
                                        comments={comments}
                                    />
                                </View>
                            </View>
                        }
                    </View>
                </ScrollView>
                {
                    this.state.isSaving &&
                    <View style={uploadStyles.blur}>
                        <View style={uploadStyles.loaderContainer}>
                            <View style={uploadStyles.downloading}>
                                <ActivityIndicator style={{ marginRight: 30, marginLeft: -30 }} size="large" color="#0000ff" />
                                <Text>{this.state.savingText}</Text>
                            </View>
                        </View>
                    </View>
                }
            </View>
        )
    }
}
export default ViewServiceOrder

