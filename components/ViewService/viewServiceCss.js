import { StyleSheet, Dimensions } from 'react-native'
let { height, width } = Dimensions.get('window');

export default StyleSheet.create({
   container: {
      //      paddingTop: 20,
      flexGrow: 1,
      flexDirection: 'column',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 20,
      marginTop: 40
   },
   margin: {
      marginBottom: 30
   },
   card: {
      padding: 10,
      backgroundColor: 'white',
      borderRadius: 10,
      width: width - 40,
      shadowColor: '#000',
      shadowOffset: { width: 10, height: 10 },
      shadowOpacity: .7,
      shadowRadius: 2,
      elevation: 5
   },
   row: {
      padding: 7,
      flexDirection: 'row',
      //           alignItems: 'center',
      //           width: width
   },

   rowCenter: {
      alignItems: 'center'
   },
   text: {
      fontSize: 15,
      flex: 1,
      flexWrap: 'wrap'
   },

   title: {
      width: 150,
      fontSize: 16,
      fontWeight: 'bold'
   },

   plantName: {
      fontWeight: 'bold'
   },

   horizontal: {
      position: 'absolute',
      top: '43%',
      left: '45%'
   },
   checkListContainer: {
      backgroundColor: 'white',
      padding: 10,
      paddingTop: 30,
      borderRadius: 5,
      width: width * .95
   },

   checkList: {
      borderColor: '#aeb7bf',
      borderWidth: 1
   },
   header: {
      backgroundColor: 'white',
      width: width / 2,
      padding: 10,
   },

   checkListTitle: {
      width: 100,
      fontWeight: 'bold'
   },

   checkListText: {
      flex: 1,
      flexWrap: 'wrap'
   },

   input: {
      padding: 7,
      width: '100%',
      borderColor: '#aeb7bf',
      borderWidth: 1,
      borderRadius: 3
   },

   punchPoint: {
      width: '100%'
   },

   updatePunch: {
      margin: 10,
      marginRight: 5,
      marginLeft: 5
   },

   submitButton: {
      backgroundColor: '#007bff',
      paddingTop: 5,
      paddingBottom: 7,
      textAlign: 'center',
      marginBottom: 15,
      //            height: 40,
      borderRadius: 5,
   },

   submitButtonText: {
      color: 'white',
      fontSize: 20,
      textAlign: 'center'
   },
   textareaContainer: {
      height: 100,
      borderRadius: 5,
      padding: 5,
      borderWidth: 1,
      borderColor: '#ced4da'
   },
   textarea: {
      textAlignVertical: 'top',  // hack android
      height: 170,
      fontSize: 14,
      color: '#333',
   },

   buttonDisabled: {
      backgroundColor: '#ccc',
      color: '#666'
   },

   buttonEnabled: {
      color: '#fff',
      backgroundColor: '#007bff'
   },
   upload: {
      width: "100%",
      marginBottom: 20,
      // position: "absolute",
      borderWidth: 1,
      borderRadius: 5,
      borderColor: "lightgray",
      padding: 10,
      backgroundColor: "#176494",
      display: 'flex',
      flexDirection: "row",
      alignItems: "center"
   },

   fileName: {
//       // whiteSpace: nowrap;
//    overflow: hidden,
//    text   : ellipsis,
// max - width: 200px;
   }


})







