
var admin = require("firebase-admin");

var serviceAccount = require("./mhub_daiva_config.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://fir-demo-cf906.firebaseio.com"
});



var topic = "sample";

let payload = {
    data: {
        title: "Testing[" + topic + "]",
        body: "Testing notification"
    }

};


  admin.messaging().sendToTopic(topic, payload)
    .then(function(response) {
      console.log("Successfully sent message:", response);
    })
    .catch(function(error) {
      console.log("Error sending message:", error);
    });