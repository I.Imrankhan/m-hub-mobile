const admin = require("firebase-admin");

const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const Port = 8080;


app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
  next();
});



app.post("/subscribe",function(req,res) {
   console.log(req.body)
  admin.messaging().subscribeToTopic(req.body.registrationToken, topic)
    .then(function(response) {
      // See the MessagingTopicManagementResponse reference documentation
      // for the contents of response.
      console.log('Successfully subscribed to topic:', response);
    })
    .catch(function(error) {
      console.log('Error subscribing to topic:', error);
    });
});



app.listen(Port, function() {
    console.log("server runs on localhost:" + Port);
  });

