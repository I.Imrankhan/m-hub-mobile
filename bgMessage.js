import firebase from 'react-native-firebase';
import {AsyncStorage, ToastAndroid} from 'react-native';
import type { RemoteMessage } from 'react-native-firebase';
import notify from './Listener'
import obj from "./components/config";

export const backgroundNotificationHandler = async (message: RemoteMessage) => {
    console.log('in backgroundNotificationHandler')
    const channel = new firebase.notifications.Android.Channel('notification-channel', 'Notification Channel', firebase.notifications.Android.Importance.Max)
        .setDescription('Notification channell');

    // Create the channel
    firebase.notifications().android.createChannel(channel);
    let msg = message
    let obj ={
        "title": msg.data.title,
        "body": msg.data.body,
        "notificationId": msg.messageId
    }
    notify(obj)
    AsyncStorage.getItem(obj.user_hCode+'notification').then((value) => {
        console.log(value)
    })
    // notification.android.setChannelId('notification-channel');
    // notification.android.setAutoCancel(true);

    // firebase.notifications().displayNotification(notification);
}