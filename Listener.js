import firebase from "react-native-firebase";
import {AsyncStorage} from "react-native";
import obj from "./components/config";


export default async function notify(notification) {
    const {title, body} = notification;
    console.log('onNotification:');
    // this.showAlert(title, body);
    // alert('message');

    const localNotification = new firebase.notifications.Notification({
        sound: 'default',
        show_in_foreground: true,
    })

        .setNotificationId(notification.notificationId)
        .setTitle(notification.title)
        .setBody(notification.body)
        // .setData(notification.data)
        .android.setChannelId('fcm_default_channel') // e.g. the id you chose above
        .android.setSmallIcon('@drawable/ic_launcher') // create this icon in Android Studio
        .android.setColor('#000000') // you can set a color here
        .android.setPriority(firebase.notifications.Android.Priority.High)
        .android.setAutoCancel(true)

    let notificationsData = [];
    let user_hCode;
    AsyncStorage.getItem('user').then((value) => {
        console.log(JSON.parse(value))
        user_hCode = JSON.parse(value).hCode

        AsyncStorage.getItem(user_hCode + 'notification').then((value) => {
            notificationsData = JSON.parse(value)
            // console.log(notificationsData)
            if (notificationsData) {
                notificationsData[notificationsData.length] = {
                    "body": notification.body,
                    "title": notification.title,
                    "created_on": new Date()

                }
            } else {
                notificationsData = []
                notificationsData[0] = {
                    "body": notification.body,
                    "title": notification.title,
                    "created_on": new Date()

                }
            }
            AsyncStorage.setItem(user_hCode + 'notification', JSON.stringify(notificationsData))
            AsyncStorage.getItem(user_hCode + 'notification').then((value) => {
            })
            if (JSON.parse(value) == null) {

            }

            // else {
            //     notifications = JSON.parse(value)
            //     notifications[notifications.length] = notification.body
            //     console.log(notifications)
            //     AsyncStorage.setItem('notification', notifications)
            // }
        })

    })


    firebase.notifications()
        .displayNotification(localNotification)
        .catch(err => console.error(err));
}
