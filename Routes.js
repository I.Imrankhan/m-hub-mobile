import React from 'react'
import {StyleSheet, AsyncStorage, Text} from 'react-native'
import { Router, Scene, Actions } from 'react-native-router-flux'
import Login from './components/Login/Login.js'
import Home from './components/Home/Home.js'
import List from './components/List/List.js'
import Assets from './components/Assets/assets.js'
import Plants from './components/Plants/Plants.js'
import ServiceOrder from './components/ServiceOrders/ServiceOrder.js'
import ShowImage from './components/Image/Image.js'
import ViewServiceOrder from './components/ViewService/viewService.js'
import val from './App.js'
import CreatePunch from './components/PunchPoints/Create/CreatePunch.js'
import ImageTaker from './components/ImageTaker/ImageTaker.js'
import obj from './components/config.js'
import NavBar from './components/NavBar/NavBar.js'
import Notifications from './components/Notifications/Notifications.js'
import Portfolio from "./components/Portfolio/Portfolio";
import TakePhoto from './components/TakePhoto/TakePhoto'

const Routes =() => {
    home = false
//    alert(obj.user_hCode)
    if(obj.user_hCode.length)
        home = true
    return (
       <Router>
          <Scene key = "root" >
             <Scene hideNavBar={true} key = "login" title = "Maintenance Hub" component = {Login}  initial = {true} />
             <Scene navBar={NavBar} key = "home" tintColor = '#4cd137' title = "" component = {Home} initial = {home} />
             <Scene key = "list" tintColor = '#4cd137' title = "" component = {List} />
             <Scene key = "assets" tintColor = '#4cd137' title = "Assets" component = {Assets}/>
             <Scene key = "plants" tintColor = '#4cd137' title = "Plants" component = {Plants} />
             <Scene key = "serviceOrder" tintColor = '#4cd137' title = "Service Order" component = {ServiceOrder} initial = {false}/>
             <Scene key = "image" tintColor = '#4cd137' title = "" component = {ShowImage} />
             <Scene key = "viewServiceOrder" tintColor = '#4cd137' title = "" component = {ViewServiceOrder} />
             <Scene key = "createPunchPoints" tintColor = '#4cd137' title = "Create Punch points" component = {CreatePunch} />
             <Scene key = "notifications" tintColor = '#4cd137' title = "Notifications" component = {Notifications} />
             <Scene key = "portfolio" tintColor = '#4cd137' title = "Portfolio" component = {Portfolio} />
             <Scene key = "takePhoto" tintColor = '#4cd137' title = "Upload Photo" component = {TakePhoto} />

          </Scene>
       </Router>
    )
}
export default Routes
