import React,{ Component } from 'react';
import { AsyncStorage, AppState, ActivityIndicator, View, Text, TouchableOpacity } from 'react-native';
import firebase from 'react-native-firebase';
import Routes from './Routes.js'
// import OfflineNotice from './components/OfflineNotice/OfflineNotice.js'
//import Home from './components/Home/Home.js'
import obj from './components/config.js'
import styles from './components/stylesCss.js'
import moment from 'moment'
import {Actions} from "react-native-router-flux";
import notify from './Listener'


class App extends Component {
    state = {
        user: '',
        isLoading: true,
        plants: [],

    }
    async componentDidMount() {

        // this.registerNotifChannels()

            // AsyncStorage.removeItem('notification')

        this.getToken();
        // this.checkPermission();
        this.createNotificationListeners(); //add this line
    }
    componentWillUnmount() {
        this.notificationListener;
        this.notificationOpenedListener;
        this.notificationDisplayedListener;

    }



    //1
    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    // registerNotifChannels() {
    //     const notifications = firebase.notifications();
    //
    //     try {
    //         const channel = new firebase.notifications.Android.Channel('app-infos', 'App Infos', firebase.notifications.Android.Importance.High)
    //             .setDescription('Demo app description')
    //             .setSound('sampleaudio.mp3');
    //         firebase.notifications().android.createChannel(channel);
    //     } catch (error) {
    //         console.log(`Error while creating notification-channel \n ${error}`);
    //     }
    // }


    async createNotificationListeners() {


        /*
        * Triggered when a particular notification has been received in foreground
        * */
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            console.log('on notiication')
            notify(notification)
        });


        const channel = new firebase.notifications.Android.Channel('fcm_default_channel', 'Demo app name', firebase.notifications.Android.Importance.High)
            .setDescription('Demo app description')
            .setSound('sampleaudio.mp3');
        firebase.notifications().android.createChannel(channel);

        /*
        * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
        * */
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            const { title, body } = notificationOpen.notification;
            console.log('onNotificationOpened:');
            Actions.notifications()
        });

        /*
        * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
        * */
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            const { title, body } = notificationOpen.notification;
            console.log('getInitialNotification:');
            Actions.notifications()
        }
        /*
        * Triggered for data only payload in foreground
        * */
        this.messageListener = firebase.messaging().onMessage((message) => {
            //process data message
            console.log('in iik imrankhan')
            let msg = JSON.parse(JSON.stringify(message))
            let obj ={
                "title": msg._data.title,
                "body": msg._data.body,
                "notificationId": msg._messageId
            }
            notify(obj)
            // notify()
        });
    }

    // 3
    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            // console.log(await firebase.messaging().getToken())
            if (fcmToken) {
                // user has a device token
                console.log('fcmToken:', fcmToken);
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        }

    }

    //2
    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.getToken();
        } catch (error) {
            // User has rejected permissions
            console.log('permission rejected');
        }
    }

   async componentWillMount() {
       await AsyncStorage.getItem('user').then((value) => {
           if(JSON.parse(value) == null) {
                this.setState({
                    isLoading: false
                })
           }

           else {
               this.setState({
                    user: JSON.parse(value).hCode,
                    isLoading: false
               })
           }
        })
    }

   render() {
       obj.user_hCode = this.state.user
       if(this.state.isLoading) {
            return (
                <View style = {styles.horizontal} >
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
       }
       return <Routes />
   }
}
export default App

